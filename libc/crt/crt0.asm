section .text
global _start
_start:
	; -------------------------------------------
	; Null stack frame
	mov rbp, 0
	push rbp
	push rbp
	mov rbp, rsp

	; -------------------------------------------
	push rsi
	push rdi
	extern _init
	call _init
	pop rdi
	pop rsi

	; -------------------------------------------
	extern main
	call main

	; -------------------------------------------
.hang:
	jmp .hang
	; Until this is done just hang
	; mov rdi, rax
	; extern exit
