$(dir $(LIB<libc>))crt%.o: $(DIR)crt%.asm
	mkdir -p $(@D)
	$(AS<amd64>) $< $(ASFLAGS<amd64>) -o $@

$(LIB<libc>): $(dir $(LIB<libc>))crti.o $(dir $(LIB<libc>))crtn.o $(dir $(LIB<libc>))crt0.o

$(SYSROOT<lib>)crt%.o: $(dir $(LIB<libc>))crt%.o
	mkdir -p $(@D)
	cp $< $@

OBJS<crt>:=$(SYSROOT<lib>)crti.o \
$(SYSROOT<lib>)crtn.o \
$(SYSROOT<lib>)crt0.o
