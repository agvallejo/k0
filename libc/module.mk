#-------------------------------------------------------------------------------
# Artifacts
#-------------------------------------------------------------------------------
DIR<libc>:=$(DIR)include/
INCLUDE<libc>:=$(DIR)include/
LIB<libc>:=$(BUILDDIR)libc/usermode/libc.a

#-------------------------------------------------------------------------------
# Source discovery
#-------------------------------------------------------------------------------
SRCS_C<libc>:=$(wildcard $(DIR)*.c)
SRCS_ASM<libc>:=$(wildcard $(DIR)*.asm)
$(eval $(call include,$(DIR)string/module.mk))
$(eval $(call include,$(DIR)stdio/module.mk))
$(eval $(call include,$(DIR)crt/module.mk))
SRCS_C<$(LIB<libc>)>:=$(SRCS_C<libc>)
SRCS_ASM<$(LIB<libc>)>:=$(SRCS_ASM<libc>)

#-------------------------------------------------------------------------------
# Rules
#-------------------------------------------------------------------------------
CC<$(LIB<libc>)>:=$(CC<target/amd64>)
AS<$(LIB<libc>)>:=$(AS<target/amd64>)
ASFLAGS<$(LIB<libc>)>:=$(ASFLAGS<target/amd64>)
CFLAGS<$(LIB<libc>)>:=$(CFLAGS<target/amd64>)
CPPFLAGS<$(LIB<libc>)>:=\
    -I$(INCLUDE<libc>) \
    -DIS_KERNEL=0 \
    -DNORETURN=_Noreturn \
    -DSTATIC=static
$(eval $(call spawn_c_rule,$(LIB<libc>)))
$(eval $(call spawn_asm_rule,$(LIB<libc>)))
$(eval $(call spawn_lib_rule,$(LIB<libc>)))
$(LIB<libc>): $(INCLUDE<sys>)

$(SYSROOT<lib>)libc.a: $(LIB<libc>)
	mkdir -p $(@D)
	cp $< $@

$(SYSROOT<include>)%.h: $(INCLUDE<libc>)%.h
	mkdir -p $(@D)
	cp $< $@

.PHONY: sysheaders
sysheaders:
	mkdir -p $(SYSROOT<include>)
	cp -ru $(INCLUDE<libc>)* $(SYSROOT<include>)
