#include <string.h>

void *memcpy(void * restrict s1, const void * restrict s2, size_t n)
{
	char *dst = s1;
	const char *src = s2;
	while(n--)
		*dst++ = *src++;
	return s1;
}
