#include <string.h>

size_t strlen(const char *s)
{
	char *t = (char*)s;
	while(*t) t++;
	return (t-s);
}
