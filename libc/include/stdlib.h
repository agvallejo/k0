#ifndef _K0_STDLIB_H
#define _K0_STDLIB_H

#include <q_libc/size_t.h>

#include <q_libc/null.h>

#define EXIT_SUCCESS 0
#define EXIT_FAILURE (-1)

void *malloc(size_t size);
void free(void *ptr);
_Noreturn void abort(void);

int atexit(void (*func)(void));
int atoi(const char *nptr);
char *getenv(const char *name);

#endif // _K0_STDLIB_H
