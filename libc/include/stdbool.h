#ifndef _K0_STDBOOL_H
#define _K0_STDBOOL_H

typedef _Bool bool;

#undef  false
#define false (0)

#undef  true
#define true (1)

#endif // _K0_STDBOOL_H
