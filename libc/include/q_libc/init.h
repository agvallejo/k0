#ifndef _K0_Q_LIBC_INIT_H
#define _K0_Q_LIBC_INIT_H

void _k0_putchar_init(void (*fun)(int));

#endif // _K0_Q_LIBC_INIT_H
