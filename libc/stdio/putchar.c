#include <stdio.h>

#include <q_libc/init.h>


#ifndef IS_KERNEL
#error "IS_KERNEL should be defined"
#elif IS_KERNEL
static void (*_k0_putchar_fun)(int c);

void _k0_putchar_init(void (*fun)(int))
{
	_k0_putchar_fun = fun;
}
#else
#include <q_libc/syscall.h>
#endif // IS_KERNEL

int putchar(int c)
{
#if IS_KERNEL
	_k0_putchar_fun(c);
#else
	syscall_1(ISR_SYSCALL_PUTCHAR, c);
#endif // IS_KERNEL
	return c;
}
