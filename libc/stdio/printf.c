#include <stdarg.h>
#include <stdio.h>

#if (IS_KERNEL == 1)
#include <stdatomic.h>
static atomic_flag printf_lock;
#define lock() while(atomic_flag_test_and_set_explicit(&printf_lock, memory_order_acquire))
#define unlock() atomic_flag_clear_explicit(&printf_lock, memory_order_release)
#else /* IS_KERNEL == 1 */
#define lock()
#define unlock()
#endif /* IS_KERNEL == 1 */

int printf(const char * restrict format, ...)
{
	int ret;

	lock();

	va_list arg;
	va_start(arg,format);
	ret = vprintf(format,arg);
	va_end(arg);

	unlock();

	return ret;
}
