#include <stdio.h>
#include <string.h>
#include <stdarg.h>

static void print(const char* data, size_t data_length){
	for (size_t i=0; i<data_length; i++)
		putchar(data[i]);
}

static int print_num2base(unsigned long long int num, unsigned int base){
	unsigned long int minor = num/base;
	int written=0;

	if(minor)
		//We need to retrieve the length of the printed string
		written=print_num2base(minor,base);

	minor = num%base;
	if(minor>9)
		putchar(minor-10+'A');
	else
		putchar(minor+'0');

	return written+1;
}

int vprintf(const char * restrict format, va_list arg)
{
	int written = 0;
	while(*format){
		if(*format!='%') {
			putchar(*format++);
			written++;
			continue;
		}
		int is_long = 0;
		while(*++format=='l' && is_long<2)
			is_long = is_long? 2 : 1;
		switch(*format) {
			case '%':
				putchar('%');
				written++;
				break;
			case 'c':
				// TODO: Widechar support
				putchar(va_arg(arg, int));
				written++;
				break;
			case 's':{
				// TODO: Widechar support
				const char* s = va_arg(arg, const char*);
				size_t len = strlen(s);
				print(s, len);
				written += len;
				break;
			}
			case 'X':
				if (is_long == 2)
					written += print_num2base(va_arg(arg,unsigned long long int), 16);
				else if (is_long == 1)
					written += print_num2base(va_arg(arg,unsigned long int), 16);
				else
					written += print_num2base(va_arg(arg,unsigned int), 16);
				break;
			case 'i':
			case 'd':
				if (is_long == 2)
					written += print_num2base(va_arg(arg, long long int), 10);
				else if (is_long == 1)
					written += print_num2base(va_arg(arg, long int), 10);
				else
					written += print_num2base(va_arg(arg, int), 10);
				break;
			case 'o':
				if (is_long == 2)
					written += print_num2base(va_arg(arg, unsigned long long int), 8);
				else if (is_long == 1)
					written += print_num2base(va_arg(arg, unsigned long int), 8);
				else
					written += print_num2base(va_arg(arg, unsigned int), 8);
				break;
			case 'u':
				if (is_long == 2)
					written += print_num2base(va_arg(arg, unsigned long long int), 10);
				else if (is_long == 1)
					written += print_num2base(va_arg(arg, unsigned long int), 10);
				else
					written += print_num2base(va_arg(arg, unsigned int), 10);
				break;
			default: //Unknown modifier. TODO: What should happen?
				putchar('%');
				putchar(*format);
		}
		format++;
	}
	return written;
}
