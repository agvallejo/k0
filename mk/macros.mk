define push =
    $(eval _SP:=S$(_SP))
    $(eval _STACK_$(_SP):=$($1))
endef
define pop =
    $(eval $1:=$(_STACK_$(_SP)))
    $(eval _SP:=$(_SP:%S=%))
endef
define include =
    $(eval $(call push,DIR))
    $(eval DIR:=$(dir $1))
    $(eval include $1)
    $(eval $(call pop,DIR))
endef
define include_mkfiles =
    $(eval $(call push,MKFILE))
    $(eval $(foreach MKFILE, $1, $(call include,$(MKFILE))))
    $(eval $(call pop,MKFILE))
endef
define envcpy =
    $(eval AS<$1>:=$(AS<$2>))
    $(eval ASFLAGS<$1>=$(ASFLAGS<$2>))
    $(eval CC<$1>:=$(CC<$2>))
    $(eval CXX<$1>:=$(CXX<$2>))
    $(eval CFLAGS<$1>:=$(CFLAGS<$2>))
    $(eval CXXFLAGS<$1>:=$(CXXFLAGS<$2>))
    $(eval CPPFLAGS<$1>:=$(CPPFLAGS<$2>))
    $(eval LDFLAGS<$1>:=$(LDFLAGS<$2>))
    $(eval LDLIBS<$1>:=$(LDLIBS<$2>))
endef
define add_to_testlist =
    $(eval TEST:=$(TESTSDIR)$(DIR)test)
    $(eval $(call envcpy,$(TEST),$1))
    $(eval TESTS+=$(TEST))
endef
define getobjs =
    $(eval OBJS<$1>:=)
    $(eval OBJS<$1>+=$(SRCS_C<$1>:%.c=$(dir $1)%.c.o))
    $(eval OBJS<$1>+=$(SRCS_CXX<$1>:%.cpp=$(dir $1)%.cpp.o))
    $(eval OBJS<$1>+=$(SRCS_ASM<$1>:%.asm=$(dir $1)%.asm.o))
endef

define spawn_asm_rule =
$(dir $1)%.asm.o: %.asm
	mkdir -p $$(@D)
	$(AS<$1>) $$< $(ASFLAGS<$1>) -o $$@
endef

define spawn_c_rule =
$(dir $1)%.c.o: %.c
	mkdir -p $$(@D)
	$(CC<$1>) $(CFLAGS<$1>) $(CPPFLAGS<$1>) -c $$< -o $$@
endef
define spawn_c_link_rule =
$(eval $(call getobjs,$1))
$1: $(OBJS<$1>) $(LDSCRIPT<$1>)
	mkdir -p $$(@D)
	$(CC<$1>) $(CFLAGS<$1>) $(LDFLAGS<$1>) $(OBJS<$1>) -o $$@ $(LDLIBS<$1>)
-include $(OBJS<$1>:.o=.d)
endef

define spawn_data_objcopy_rule =
$(eval $(call getobjs,$1))
$1: $(OBJS<$1>)
	mkdir -p $$(@D)
	llvm-objcopy $$^ --dump-section .data=$1
-include $(OBJS<$1>:.o=.d)
endef

define spawn_cxx_rule =
$(dir $1)%.cpp.o: %.cpp
	mkdir -p $$(@D)
	$(CXX<$1>) $(CXXFLAGS<$1>) $(CPPFLAGS<$1>) -c $$< -o $$@
endef
define spawn_cxx_link_rule =
$(eval $(call getobjs,$1))
$1: $(OBJS<$1>) $(LDSCRIPT<$1>)
	mkdir -p $$(@D)
	$(CXX<$1>) $(CXXFLAGS<$1>) $(LDFLAGS<$1>) $(OBJS<$1>) -o $$@ $(LDLIBS<$1>)
-include $(OBJS<$1>:.o=.d)
endef

define spawn_lib_rule =
$1: $(OBJS<$1>)
	mkdir -p $$(@D)
	$(AR) rcs $$@ $$^
-include $(OBJS<$1>:.o=.d)
endef
