DIR:=mk/
# Useful macros to be used with $(call ...)
#
# All inclludes should go through the 'include' macro, but we don't have it yet.
# This should be the only exception of an include that uses an absolute path
include mk/macros.mk

$(eval $(call include,$(DIR)config.mk))

# Init the tests to run
TESTS:=
