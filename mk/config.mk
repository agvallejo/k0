BUILDDIR:=build/
TESTSDIR:=$(BUILDDIR)tests/

CWARNINGS<default>:=-Wstrict-prototypes -Wmissing-prototypes -Wmisleading-indentation
WARNINGS<default>:=-Wall -Wextra -Werror -Wundef -pedantic -Wno-missing-braces

OPTIMISATION?=-O3

ASFLAGS<default>:=-Werror
CFLAGS<default>:=-std=c18 $(WARNINGS<default>) $(CWARNINGS<default>)
CXXFLAGS<default>:=-std=c++17 $(WARNINGS<default>)
CPPFLAGS<default>:=-MD -MP $(OPTIMISATION)
LDFLAGS<default>:=
LDLIBS<default>:=

# Native environment for tests
AS<test/native>:=nasm
CC<test/native>:=clang
ASFLAGS<test/native>:=$(ASFLAGS<default>) \
    -felf64
CFLAGS<test/native>:=$(CFLAGS<default>)
CXXFLAGS<test/native>:=-$(CXXFLAGS<default>)
CPPFLAGS<test/native>:=$(CPPFLAGS<default>) \
    -DSTATIC= \
    -DNORETURN= \
    -DEXTERN=extern \
    -DTESTING
LDFLAGS<test/native>:=$(LDFLAGS<default>)
LDLIBS<test/native>:=$(LDLIBS<default>)

# SystemV-style for kernel itself
AS<target/amd64>:=nasm
CC<target/amd64>:=clang \
    -target x86_64-unknown-gnu
ASFLAGS<target/amd64>:=$(ASFLAGS<default>) \
    -felf64
CFLAGS<target/amd64>:=$(CFLAGS<default>)
CXXFLAGS<target/amd64>:=-$(CXXFLAGS<default>)
CPPFLAGS<target/amd64>:=$(CPPFLAGS<default>) \
    -DSTATIC=static \
    -DNORETURN=_Noreturn \
    -DEXTERN=
LDFLAGS<target/amd64>:=$(LDFLAGS<default>) \
    -fuse-ld=lld \
    -Wl,-no-dynamic-linker \
    -Wl,-fatal-warnings
LDLIBS<target/amd64>:=$(LDLIBS<default>)

# Win64-style for EFI apps
AS<target/win64>:=nasm
CC<target/win64>:=clang -target x86_64-pc-win32-coff
ASFLAGS<target/win64>:=$(ASFLAGS<default>) \
    -fwin64
CFLAGS<target/win64>:=$(CFLAGS<default>)
CXXFLAGS<target/win64>:=-$(CXXFLAGS<default>)
CPPFLAGS<target/win64>:=$(CPPFLAGS<default>) \
    -DSTATIC=static \
    -DNORETURN=_Noreturn \
    -DEXTERN=
LDFLAGS<target/win64>:=$(LDFLAGS<default>) \
    -fuse-ld=lld-link \
    -Wl,-subsystem:efi_application
LDLIBS<target/win64>:=$(LDLIBS<default>)


