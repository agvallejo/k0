# Environment for testing using munit
DIR<munit>:=$(DIR)munit/
SRCS_C<munit>:=$(DIR<munit>)munit.c

CC<munit>:=$(CC<test/native>)
CXX<munit>:=$(CXX<test/native>)
AS<munit>:=$(AS<test/native>)
ASFLAGS<munit>:=$(ASFLAGS<test/native>)
CFLAGS<munit>:=$(CFLAGS<test/native>)
CXXFLAGS<munit>:=$(CXXFLAGS<test/native>)
CPPFLAGS<munit>:=$(CPPFLAGS<test/native>) \
	-DMUNIT_NO_FORK \
	-I$(DIR<munit>)
LDFLAGS<munit>:=$(LDFLAGS<test/native>)
LDLIBS<munit>:=$(LDLIBS<test/native>)
