#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>

#include <efi.h>

#include <k0/boot.h>
#include <k0/kassert.h>

#include <efi-bs.h>
#include "efi_trampoline.h"
#include "efi_vmm.h"

extern EFI_SYSTEM_TABLE *gST;
extern void *efi_trampoline_bsp_asm_start;
extern void *efi_trampoline_bsp_asm_end;
extern void *efi_trampoline_ap_asm_start;
extern void *efi_trampoline_ap_asm_end;

uintptr_t efi_trampoline(struct hbt **hbt)
{
	uintptr_t bsp_start = (uintptr_t)&efi_trampoline_bsp_asm_start;
	uintptr_t bsp_end   = (uintptr_t)&efi_trampoline_bsp_asm_end;
	uintptr_t ap_start  = (uintptr_t)&efi_trampoline_ap_asm_start;
	uintptr_t ap_end    = (uintptr_t)&efi_trampoline_ap_asm_end;

	/* The HBT must fit in the page */
	size_t size = bsp_end-bsp_start+ap_end-ap_start+sizeof **hbt;
	kassert(size, <, VMM_PAGESIZE);

	/* Allocate a single page below the 1MiB mark. We want to
         * be supersure that the trampoline is way outside the kernel's
	 * memory map and is usable with the APs real mode trampoline
	 *
	 * The MP Spec. says that 0xVV000 where VV=A0-B0 are reserved
	 * It's probably alright, but it's no biggie requesting below that
	 */
	uintptr_t trampoline = 0x9F000;
	kassert(EFI_SUCCESS, ==, gST->BootServices->AllocatePages(AllocateMaxAddress,
	                                                       EfiLoaderData,
	                                                       1, (void*)&trampoline));

	/* Note that we can't jump to the trampoline in this image
	 * as the image mmight be in the kernel's memory map.
         * We need to map the trampoline in the new PML4 and then
	 * jump to it.
	 */
	efi_vmm_map(trampoline, trampoline, VMM_PAGESIZE, EFI_PTYPE_NON_RECLAIMABLE);
	uintptr_t cur_pos = trampoline;

	/* Copy the trampoline to the start of the page */
	memcpy((void*)cur_pos, (void*)ap_start, ap_end-ap_start);
	cur_pos +=ap_end-ap_start;

	/* Copy the trampoline to the start of the page */
	memcpy((void*)cur_pos, (void*)bsp_start, bsp_end-bsp_start);
	cur_pos +=bsp_end-bsp_start;

        /* Locate the handover table at the tail of the page */
        *hbt = (void*)(trampoline+VMM_PAGESIZE-sizeof **hbt);

	return trampoline+ap_end-ap_start;
}
