#ifndef EFI_ELF_PRIVATE_H
#define EFI_ELF_PRIVATE_H

#include <stdint.h>
#include "efi_elf.h"

/* Indices into e_ident[] */
#define EI_MAG0       (0U)  /** 1st index for the magic word */
#define EI_MAG1       (1U)  /** 2nd index for the magic word */
#define EI_MAG2       (2U)  /** 3rd index for the magic word */
#define EI_MAG3       (3U)  /** 4th index for the magic word */
#define EI_CLASS      (4U)  /** File class */
#define EI_DATA       (5U)  /** Data encoding */
#define EI_VERSION    (6U)  /** File version */
#define EI_OSABI      (7U)  /** OS/ABI identification */
#define EI_ABIVERSION (8U)  /** ABI version */
#define EI_PAD        (9U)  /** Start of padding bytes */
#define EI_NIDENT     (16U) /** Size of e_ident[] */

/* ELF identification constants */
#define ELFMAG0     (0x7FU)
#define ELFMAG1     ('E')
#define ELFMAG2     ('L')
#define ELFMAG3     ('F')
#define ELFCLASS64  (2U)
#define ELFDATA2LSB (1U)
#define EM_X86_64   (62U)

/* Header of an ELF image */
struct elf_ehdr {
	uint8_t   e_ident[EI_NIDENT];
	uint16_t  e_type;
	uint16_t  e_machine;
	uint32_t  e_version;
	uintptr_t e_entry; /* Different for 32/64 bits architectures */
	uintptr_t e_phoffset; /* Different for 32/64 bits architectures */
	uintptr_t e_shoffset; /* Different for 32/64 bits architectures */
	uint32_t  e_flags;
	uint16_t  e_ehsize;
	uint16_t  e_phentsize;
	uint16_t  e_phnum;
	uint16_t  e_shentsize;
	uint16_t  e_shnum;
	uint16_t  e_shstrndx;
};

struct elf_phdr {
	uint32_t p_type;
#define PT_LOAD (1U)
#if UINTPTR_MAX == 0xFFFFFFFFFFFFFFFF
	uint32_t p_flags;
#define PF_X (1U)
#define PF_W (2U)
#define PF_R (3U)
	uint64_t p_offset;
	uint64_t p_vaddr;
	uint64_t p_paddr;
	uint64_t p_filesz;
	uint64_t p_memsz;
	uint64_t p_align;
#elif UINTPTR_MAX == 0xFFFFFFFF
	uint32_t p_offset;
	uint32_t p_vaddr;
	uint32_t p_paddr;
	uint32_t p_filesz;
	uint32_t p_memsz;
	uint32_t p_flags;
	uint32_t p_align;
#else
#error "Unexpected word size"
#endif
};

#endif // EFI_ELF_PRIVATE_H
