DIR<efi_loader>:=$(DIR)
OVMF<efi_loader>:=$(DIR<efi_loader>)ovmf/
BIN<efi_loader>:=$(BUILDDIR)efi_loader/boot.efi
IMG<efi_loader>:=$(BUILDDIR)efi_loader/efi.img
CREATE_IMG<efi_loader>:=$(DIR)create_img.sh

SRCS_C<$(BIN<efi_loader>)>:=$(wildcard $(DIR)*.c) $(SRCS_C<libc>) $(SRCS_C<pyocap>)
SRCS_ASM<$(BIN<efi_loader>)>:=$(wildcard $(DIR)*.asm)

# UEFI uses PE files instead of ELFs
CC<$(BIN<efi_loader>)>:=$(CC<target/win64>)
CFLAGS<$(BIN<efi_loader>)>:=$(CFLAGS<target/win64>) \
         -ffreestanding \
         -fno-stack-protector \
         -mno-red-zone \
         -fshort-wchar \
         -mno-sse
CPPFLAGS<$(BIN<efi_loader>)>:=$(CPPFLAGS<target/win64>) \
    -I$(DIR)include \
    -I$(INCLUDE<kernel>) \
    -I$(INCLUDE<libc>) \
    -I$(INCLUDE<pyocap>) \
    -DIS_KERNEL=1
AS<$(BIN<efi_loader>)>:=$(AS<target/win64>)
ASFLAGS<$(BIN<efi_loader>)>:=$(ASFLAGS<target/win64>)
LDFLAGS<$(BIN<efi_loader>)>:=$(LDFLAGS<target/win64>) \
    -Wl,-entry:efi_main
LDLIBS<$(BIN<efi_loader>)>:=-nostdlib
$(eval $(call spawn_c_rule,$(BIN<efi_loader>)))
$(eval $(call spawn_asm_rule,$(BIN<efi_loader>)))
$(eval $(call spawn_c_link_rule,$(BIN<efi_loader>)))


$(IMG<efi_loader>): $(CREATE_IMG<efi_loader>) $(BIN<efi_loader>) $(BIN<kernel>)
	KERNEL_BIN=$(BIN<kernel>) EFI_IMG=$(IMG<efi_loader>) EFI_APP=$(BIN<efi_loader>) $(CREATE_IMG<efi_loader>)
