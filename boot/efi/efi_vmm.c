#include <k0/kassert.h>

#include <efi.h>

#include "efi_vmm.h"
#include "efi_pmm.h"

#include <string.h>
#include <stddef.h>
#include <stdio.h>

#define PTABLES_ENTRIES (512u)

extern EFI_SYSTEM_TABLE *gST;
static uintptr_t *pml4;

#ifndef EFI_VMM_DEBUG
#define puts(x)
#define printf(...)
#endif

static uintptr_t *efi_vmm_table_alloc(void)
{
	uintptr_t *ret = NULL;
	kassert(EFI_SUCCESS, ==, gST->BootServices->AllocatePages(AllocateAnyPages,
	                                                          EfiLoaderData,
	                                                          1, (void*)&ret));

	memset(ret, 0, VMM_PAGESIZE);

	/* Don't let the kernel use memory already in use to hold page tables! */
	efi_pmm_reserve((uintptr_t)(void*)ret, VMM_PAGESIZE);
	return ret;
}

static uintptr_t *efi_vmm_getentry(uintptr_t vaddr, int lvl)
{
	uintptr_t *entry = NULL;
	uintptr_t *table = pml4;

	puts("Finding entry");
	for (int i=0; i<=lvl; i++) {
		kassert_notnull(table, vaddr|i);
		size_t idx =(vaddr>>(39-9*i))&(0x1FF);
		entry = (void*)&table[idx];
		printf("    lvl=%d, table=%llX, idx=%d, entry=%llX\n", i, table, idx, *entry);
		table = (uintptr_t*)(~(VMM_AMD64_PAGESIZE_PT-1) & (uintptr_t)*entry);
	}
	return entry;
}

static void efi_vmm_map_one(uintptr_t vaddr, uintptr_t paddr)
{
	size_t lvl;
	uintptr_t *entry;
	printf("Mapping %llX\n", vaddr);
	for (lvl=0; lvl<3; lvl++) {
		entry = efi_vmm_getentry(vaddr, lvl);
		if (!*entry) {
			*entry = (uintptr_t)efi_vmm_table_alloc();
			*entry |= VMM_AMD64_RW | VMM_AMD64_PRESENT;
			printf("allocated %llX\n", *entry);
		}
	}
	entry = efi_vmm_getentry(vaddr, lvl);
	kassert_notnull(!*entry, (uintptr_t)entry);
	*entry = paddr | VMM_AMD64_RW | VMM_AMD64_PRESENT;
}

uintptr_t efi_vmm_init(void)
{
	pml4 = efi_vmm_table_alloc();
	pml4[510] = (uintptr_t)pml4;
	pml4[510] |= VMM_AMD64_NX | VMM_AMD64_RW | VMM_AMD64_PRESENT;
	return (uintptr_t)pml4;
}

void efi_vmm_map(uintptr_t vaddr, uintptr_t paddr, size_t len, enum efi_ptype type)
{
	if (!len)
		return;

	switch (type)
	{
	case EFI_PTYPE_NON_RECLAIMABLE:
		/* Careful here. This is either the kernel, the trampoline or
		 * something similar that should outlive the EFI loader.
		 * When the kernel boots it will read the memory map, but it
		 * needs to skip itself and the HBT (at least). This type
		 * allows us to tell the kernel which usable RAM areas it
		 * should NOT use immediately until the bootstrap is complete
		 */
		efi_pmm_reserve(paddr, len);
		break;
	case EFI_PTYPE_RECLAIMABLE:
		/* Allow the kernel to use this memory immediately */
	case EFI_PTYPE_MMIO:
		/* Don't do anything special. It won't be in the memory map anyway */
		break;
	}

	for (size_t i=0; i<((len+VMM_AMD64_PAGESIZE_PT-1)/VMM_AMD64_PAGESIZE_PT); i++)
		efi_vmm_map_one(vaddr+i*VMM_AMD64_PAGESIZE_PT,
		                paddr+i*VMM_AMD64_PAGESIZE_PT);
}

void efi_vmm_alloc(uintptr_t vaddr, size_t len)
{
	if (!len)
		return;
	for (size_t i=0; i<((len+VMM_AMD64_PAGESIZE_PT-1)/VMM_AMD64_PAGESIZE_PT); i++)
		efi_vmm_map_one(vaddr+i*VMM_AMD64_PAGESIZE_PT,
		                (uintptr_t)efi_vmm_table_alloc());
}
