#ifndef QUEVEDO_EFI_TRAMPOLINE_H
#define QUEVEDO_EFI_TRAMPOLINE_H

/**
 * Call the trampoline
 *
 * It will relocate the actual trampoline out of
 * the kernel's way. The trampoline
 * will set cr3 (with the trampoline mapped) and
 * jump to the entry point. After that, we're done
 * And the boot takes place in the kernel proper
 *
 * @return address of the trampoline
 */
struct hbt;
uintptr_t efi_trampoline(struct hbt **hbt);

#endif /* QUEVEDO_EFI_TRAMPOLINE_H */
