#ifndef QUEVEDO_EFI_ACPI_PRIVATE_H
#define QUEVEDO_EFI_ACPI_PRIVATE_H

#include "efi_acpi.h"

#define ACPI_MADT_ENTRY_PROCESSOR_LAPIC (0U)

struct acpi_rsdp {
	uint8_t  signature[8];
	uint8_t  checksum;
	uint8_t  oemid[6];
	uint8_t  revision;
	uint32_t rsdt_addr;
	uint32_t length;
	uint64_t xsdt_addr;
	uint8_t  ext_checksum;
	uint8_t  rsvd[3];
} __attribute__((packed));

struct acpi_sdt_hdr {
	uint8_t  signature[4];
	uint32_t length;
	uint8_t  revision;
	uint8_t  checksum;
	uint8_t  oemid[6];
	uint8_t  oem_table_id[8];
	uint32_t oem_revision;
	uint32_t creator_id;
	uint32_t creator_revision;
} __attribute__((packed));

struct acpi_rsdt {
	struct acpi_sdt_hdr hdr;
	uint32_t entry[];
} __attribute__((packed));

struct acpi_xsdt {
	struct acpi_sdt_hdr hdr;
	uint64_t entry[];
} __attribute__((packed));

struct acpi_hpet {
	struct acpi_sdt_hdr hdr;
	uint8_t  hw_rev_id;
	uint8_t  flags;
	uint16_t pci_vendor;
	uint8_t  addr_space_id;
	uint8_t  reg_bitwidth;
	uint8_t  reg_bitoffset;
	uint8_t  rsvd;
	uint64_t base_addr;
	uint8_t  hpet_num;
	uint16_t min_tick;
	uint8_t  protection;
} __attribute__((packed));

struct acpi_madt_entry{
	uint8_t type;
	uint8_t length;
	union {
		struct {
			uint8_t  proc_id;
			uint8_t  apic_id;
			uint32_t flags;
		}__attribute__((packed)) lapic;
		struct {
			uint8_t  ioapic_id;
			uint8_t  rsvd;
			uint32_t ioapic_addr;
			uint32_t glb_sys_int_base;
		}__attribute__((packed)) ioapic;
		struct {
			uint8_t  bus_src;
			uint8_t  irq_src;
			uint32_t glb_sys_int;
			uint16_t flags;
		}__attribute__((packed)) int_override;
		struct {
			uint8_t  proc_id;
			uint16_t flags;
			uint8_t  lint;
		}__attribute__((packed)) nmi;
		struct {
			uint16_t rsvd;
			uint64_t addr;
		}__attribute__((packed)) lapic_addr;
	};
}__attribute__((packed));

_Static_assert(6  == sizeof ((struct acpi_madt_entry*)0)->lapic,        "Size mismatch");
_Static_assert(10 == sizeof ((struct acpi_madt_entry*)0)->ioapic,       "Size mismatch");
_Static_assert(8  == sizeof ((struct acpi_madt_entry*)0)->int_override, "Size mismatch");
_Static_assert(4  == sizeof ((struct acpi_madt_entry*)0)->nmi,          "Size mismatch");
_Static_assert(10 == sizeof ((struct acpi_madt_entry*)0)->lapic_addr,   "Size mismatch");

struct acpi_madt {
	struct acpi_sdt_hdr hdr;
	uint32_t lapic_addr;
	uint32_t flags;
	/* IMPORTANT: The entries are not actually aligned in an array, but packed */
	struct acpi_madt_entry entry[];
} __attribute__((packed));

#endif /* QUEVEDO_EFI_ACPI_PRIVATE_H */
