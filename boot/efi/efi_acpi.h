#ifndef QUEVEDO_EFI_ACPI_H
#define QUEVEDO_EFI_ACPI_H

/**
 * Find ACPI tables 
 *
 * Performs initialisation tasks and returns important info
 * back to the OS
 */
struct hbt;
void efi_acpi_init(struct hbt *hbt);

#endif /* QUEVEDO_EFI_ACPI_H */
