#ifndef QUEVEDO_EFI_VMM_H
#define QUEVEDO_EFI_VMM_H

#include <stdint.h>
#include <stddef.h>

#define VMM_AMD64_PAGESIZE_PT (4llu<<10)
#define VMM_AMD64_PRESENT (1u)
#define VMM_AMD64_RW      (2u)
#define VMM_AMD64_NX      (1llu<<63)
#define VMM_MASKSIZE (9u)
#define VMM_MASKSIZE_PT (12u)
#define VMM_PAGESIZE (4096u)

/**
 * Initialise the pre-boot virtual memory manager
 *
 * It doesn't actually control the current memory
 * map, jsut operates on a new PML4 that is not
 * yet applied. It won't come into effect until
 * the very end of the boot process, before the
 * handover to the kernel init services
 *
 * @param pml4 of the to-be-booted system
 */
uintptr_t efi_vmm_init(void);

enum efi_ptype
{
	/** Not actual RAM. Don't send use it as such */
	EFI_PTYPE_MMIO,

	/** Reclaimable by the kernel as soon as possible */
	EFI_PTYPE_RECLAIMABLE,
	/**
	 * Non-immediately reclaimable and in use for the global paging
	 * structures and to hold the HBT. Not that it's special or
	 * or anything, but it's best if the kernel doesn't use its
         * own paging backbone. You know, for the sake of preventing
	 * needless computation by the kernel. It's otherwise normal
         * memory and can be freed normally when destryoing the address
	 * space.
	 */
	EFI_PTYPE_NON_RECLAIMABLE
};

/**
 * Map a series of physical addresses into the virtual map
 *
 * @param vaddr Virtual address on the not yet loaded address space
 * @param paddr Physical address to back it up
 * @param len Size in octets (will be round up to pages)
 * @param ptype Type of physical page anything the kernel should reclaim
 *              is EFI_PTYPE_RECLAIMABLE, else EFI_PTYPE_NON_RECLAIMABLE
 */
void efi_vmm_map(uintptr_t vaddr, uintptr_t paddr, size_t len, enum efi_ptype);

/**
 * Allocate 'len' free memory at vaddr
 *
 * Allocates and maps free pages into 'vaddr'
 *
 * @param vadddr Where to map the pages
 * @param len Size of the virtual allocation
 */
void efi_vmm_alloc(uintptr_t vaddr, size_t len);

#endif /* QUEVEDO_EFI_VMM_H */
