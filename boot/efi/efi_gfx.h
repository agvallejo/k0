#ifndef QUEVEDO_EFI_GFX_H
#define QUEVEDO_EFI_GFX_H

#include <efi.h>

/** Enable graphics on an IF */
struct hbt_gfx;
void efi_gfx_enable(struct hbt_gfx *gfx);
	
#endif /* QUEVEDO_EFI_GFX_H */
