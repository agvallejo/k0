#ifndef QUEVEDO_EFI_PMM_H
#define QUEVEDO_EFI_PMM_H

#include <efi.h>

#include <stdint.h>
#include <stddef.h>

struct hbt_pmm;
UINTN efi_pmm_init(struct hbt_pmm *pmm);

/**
 * Reserve a memory region so the kernel can skip it
 * while parsing the memory map
 *
 * @param addr Start of the region
 * @param len Size of the region
 */
void efi_pmm_reserve(uintptr_t addr, size_t len);

#endif /* QUEVEDO_EFI_PMM_H */
