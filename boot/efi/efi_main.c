#include <k0/boot.h>
#include <k0/kassert.h>
#include <q_libc/init.h>

#include <efi.h>

#include "efi_acpi.h"
#include "efi_elf.h"
#include "efi_fs.h"
#include "efi_gfx.h"
#include "efi_pmm.h"
#include "efi_trampoline.h"
#include "efi_vmm.h"

#include <stdio.h>

#define KERNEL_FILENAME ((CHAR16*)L"\\EFI\\k0\\k0.elf")
#define INITRD_FILENAME ((CHAR16*)L"\\EFI\\k0\\initrd.bin")

EFI_SYSTEM_TABLE *gST = NULL;

/**
 * Putchar-like interface
 *
 * Provide libc-style putchar to get full-blown printf
 *
 * @param c input character
 */
static void efi_putc(int c)
{
	CHAR16 msg[3] = {0};
	if (c == '\n') {
		msg[0] = '\r';
		msg[1] = '\n';
	}
	else
		msg[0] = c;
	
	gST->ConOut->OutputString(gST->ConOut, msg);
}

EFI_STATUS efi_main(EFI_HANDLE handle, EFI_SYSTEM_TABLE *systab); /* Keep lint happy */
EFI_STATUS efi_main(EFI_HANDLE handle, EFI_SYSTEM_TABLE *systab)
{
	/* Initialise stdout to use UEFI's text capabilities */
	_k0_putchar_init(efi_putc);

	/* Make the System Table global  out of convenience for libc */
	gST = systab;

	puts("Starting k0 Boot Procedure");

	/* Bootstrap the PML4 manager */
	uintptr_t pml4 = efi_vmm_init();

	/* Bring the kernel into memory */
	uintptr_t kernel_start = 0;
	size_t kernel_size = 0;
	efi_fs_read(handle, KERNEL_FILENAME, &kernel_size, &kernel_start);

	uintptr_t initrd_start = 0;
	size_t initrd_size = 0;
#if 0
	efi_fs_read(handle, INITRD_FILENAME, &initrd_size, &initrd_start);
	efi_pmm_reserve(initrd_start, VMM_PAGESIZE*((initrd_size+VMM_PAGESIZE-1)/VMM_PAGESIZE));
#endif

	uintptr_t entry = efi_elf_load(kernel_start);

	/* Get a trampoline out of the kernel's way */
	struct hbt *hbt;
	uintptr_t trampoline = efi_trampoline(&hbt);
	*hbt = (struct hbt)
	{
		.id         = K0_HBT_ID,
		.version    = K0_HBT_VERSION,
		.initrd =
		{
			.init =
			{
				.start = initrd_start,
				.octets = VMM_PAGESIZE*((initrd_size+VMM_PAGESIZE-1)/VMM_PAGESIZE)
			},
		},
		.ap_params.trampoline = trampoline & ~(VMM_PAGESIZE-1),
		.ap_params.cr3 = pml4
	};

	/* Find ACPI tables */
	efi_acpi_init(hbt);

	/* Enable the pixel-framebuffer */
	efi_gfx_enable(&hbt->gfx);

	UINTN map_key = efi_pmm_init(&hbt->pmm);
	/* Exit Boot Services */
	kassert(EFI_SUCCESS, ==, gST->BootServices->ExitBootServices(handle, map_key));

	/* Jump on the trampoline! */
	__asm__ volatile ("jmpq *%0": :"r"(trampoline), "a"(entry), "b"(pml4), "c"(K0_MAGIC), "d"(hbt));

	return EFI_SUCCESS;
}

