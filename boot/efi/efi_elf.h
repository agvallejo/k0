#ifndef QUEVEDO_EFI_ELF_H
#define QUEVEDO_EFI_ELF_H

/**
 * Load an ELF binary into memory
 *
 * @param addr Location of the ELF
 * @return Entry point to the file
 */
uintptr_t efi_elf_load(uintptr_t addr);

#endif /* QUEVEDO_EFI_ELF_H */
