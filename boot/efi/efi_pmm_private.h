#ifndef QUEVEDO_EFI_PMM_PRIVATE_H
#define QUEVEDO_EFI_PMM_PRIVATE_H

#include "efi_pmm.h"

struct efi_pmm_data
{
	struct {
		uint64_t start;
		uint64_t octets;
	} rsvd_pages[64]; /* Pages in use by the kernel itself */
	size_t rsvd_pages_size; /* Next position in \c reserved_pages */
};

#endif /* QUEVEDO_EFI_PMM_PRIVATE_H */
