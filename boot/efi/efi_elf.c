#include <k0/kassert.h>

#include <stdio.h>
#include <string.h>

#include "efi_vmm.h"
#include "efi_elf_private.h"

static void elf_validate(struct elf_ehdr *ehdr)
{
	kassert(ehdr->e_ident[EI_MAG0],  ==, ELFMAG0);
	kassert(ehdr->e_ident[EI_MAG1],  ==, ELFMAG1);
	kassert(ehdr->e_ident[EI_MAG2],  ==, ELFMAG2);
	kassert(ehdr->e_ident[EI_MAG3],  ==, ELFMAG3);
	kassert(ehdr->e_ident[EI_CLASS], ==, ELFCLASS64);
	kassert(ehdr->e_ident[EI_DATA],  ==, ELFDATA2LSB);
	kassert(ehdr->e_machine, ==, EM_X86_64);
}

uintptr_t efi_elf_load(uintptr_t addr) {
	struct elf_ehdr *ehdr = (void*)addr;
	elf_validate(ehdr);

	struct elf_phdr *phdr = (void*)((uintptr_t)addr+ehdr->e_phoffset);
	size_t count = ehdr->e_phnum;
	while(count--) {
		if (phdr->p_type == PT_LOAD) {
			uintptr_t page_start = 0x1000*(phdr->p_vaddr/0x1000);
			uintptr_t page_bss_start = 0x1000*((phdr->p_vaddr+phdr->p_filesz+0x1000-1)/0x1000);
			uintptr_t page_end   = 0x1000*((phdr->p_vaddr + phdr->p_memsz+0x1000-1)/0x1000);

			efi_vmm_map(page_start, addr+phdr->p_offset, page_bss_start-page_start, EFI_PTYPE_NON_RECLAIMABLE);
			efi_vmm_alloc(page_bss_start, page_end-page_bss_start);
		}
		phdr = (void*)((uintptr_t)phdr+ehdr->e_phentsize);
	}

	return ehdr->e_entry;
}
