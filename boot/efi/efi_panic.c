#include <stdio.h>
#include <stddef.h>

#include <k0/kassert.h>
#include <k0/hal.h>

void panic_full(char *filename, char *line, char *sarg1, char *op, char *sarg2, uint64_t arg1, uint64_t arg2)
{
	printf("\nPANIC! %s-line=%s-(%s%s%s)(0x%lX, 0x%lX)\n", filename, line, sarg1, op, sarg2, arg1, arg2);
	halt_forever();
}

_Noreturn void panic_simple(char *filename, char *line, char *cond, uint64_t arg)
{
	printf("\nPANIC! %s-line=%s-%s-arg=0x%lX\n", filename, line, cond, arg);
	halt_forever();
}
