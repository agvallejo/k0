include mk/init.mk

$(eval $(call include,test_framework/module.mk))
$(eval $(call include,libc/module.mk))
$(eval $(call include,kernel/module.mk))
$(eval $(call include,usr/module.mk))
$(eval $(call include,pyocap/module.mk))
$(eval $(call include,boot/module.mk))

.DEFAULT_GOAL:=qemu

.PHONY: qemu
NCORES?=`nproc`
qemu: $(IMG<efi_loader>) $(wildcard $(OVMF<efi_loader>)*.fd)
	qemu-system-x86_64 -cpu host,migratable=no -s -enable-kvm \
	-smp $(NCORES) \
	-drive if=pflash,format=raw,unit=0,file=$(OVMF<efi_loader>)OVMF_CODE.fd,readonly=on \
	-drive if=pflash,format=raw,unit=1,file=$(OVMF<efi_loader>)OVMF_VARS.fd \
	-net none \
	-drive file=$(IMG<efi_loader>),if=ide

.PHONY: all
all: $(IMG<efi_loader>) $(IMG<initrd>)

.PHONY: test
test: $(TESTS)
	$(^:= &&) true

.PHONY: clean
clean:
	rm -rf $(BUILDDIR)
