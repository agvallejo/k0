from pyutils import *

class Obj:
    def __init__(self, oid=None):
        self.oid = oid

    def save(self, outfile, indent=""):
        outfile.write(indent+f".hdr.oid={self.oid},\n")

class Cap:
    def __init__(self, obj, type, guard=0):
        self.restr = cr_None
        self.type  = type
        self.obj   = obj
        self.guard = guard
        self.oid   = obj.oid if obj else None

    def __setitem__(self, slotnum, cap):
        # This is basically so we can index GPT caps as
        # if they were GPT objects and do cap[3][2][5]
	# If you try this with any other cap it will
	# blow up
        self.obj[slotnum] = cap

    def __getitem__(self, slotnum):
        # This is basically so we can index GPT caps as
        # if they were GPT objects and do cap[3][2][5]
	# If you try this with any other cap it will
	# blow up
        return self.obj[slotnum]

    def dump(self, indent=""):
        print(indent+f"type={self.type}")
        if self.obj:
            print(indent+f"oid={self.oid}")
            self.obj.dump(indent)

    def get_restr(self):
        if self.restr == cr_None:
            return 'cr_None'

        restr_str = []
        if self.restr & cr_Opaque:
            restr_str.append('cr_Opaque')
        if self.restr & cr_ReadOnly:
            restr_str.append('cr_ReadOnly')
        if self.restr & cr_Weak:
            restr_str.append('cr_Weak')
        if self.restr & cr_NoExec:
            restr_str.append('cr_NoExec')
        return '|'.join(restr_str)

    def save(self, outfile, indent=""):
        if self.type == 'ct_GPT':
            union = 'gpt'
        elif self.type == 'ct_Page':
            union = 'page'
        elif self.type == 'ct_Console':
            union = 'any'
        else:
            union = 'obj'

        if union != 'any':
            outfile.write(indent+f".{union}.oid   = {self.oid},\n")
        outfile.write(indent+f".{union}.type  = {self.type},\n")
        outfile.write(indent+f".{union}.restr = {self.get_restr()}")
        if union == 'gpt':
            outfile.write(',\n')
            outfile.write(indent+f".gpt.guard = {hex(self.guard)},\n")
            outfile.write(indent+f".gpt.slot_bitsize = {self.obj.l2size_child}\n")
        elif union == 'page':
            outfile.write(',\n')
            outfile.write(indent+f".page.guard = {hex(self.guard)}\n")
        else:
            outfile.write('\n')

