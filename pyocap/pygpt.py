from pyutils import *
from pyocap import *

class GPT(Obj):
    def __init__(self, l2size, oid=None):
        super().__init__(oid)
        self.l2size = l2size
        self.size = 1<<self.l2size
        self.l2size_child = l2size-GPT_BITS
        self.size_child = 1<<self.l2size_child
        self.slot = GPT_SIZE*[None]

    def __setitem__(self, slotnum, cap):
        self.slot[slotnum] = cap

    def __getitem__(self, slotnum):
        return self.slot[slotnum]

    def cap(self, guard):
        return Cap(self, 'ct_GPT', guard)

    def dump(self, indent=""):
        print(indent+f"l2size={self.l2size}")
        indent += "    "
        for i, cap in enumerate(self.slot):
            if cap:
                print(indent+f"i={i}")
                print(indent+f"guard={hex(cap.guard)}")
                cap.dump(indent)

    def save(self, outfile, indent=""):
        super().save(outfile, indent=indent)
        outfile.write(indent+ ".cap = {\n")
        indent += '    '

        nslots = 0
        for i, slot in enumerate(self.slot):
            if slot:
                if nslots > 0:
                    outfile.write(",\n")
                nslots += 1
                outfile.write(indent+f"[{i}] = {{\n")
                slot.save(outfile, indent+4*' ')
                outfile.write(indent+"}")

        indent = indent[:-4]
        outfile.write("\n"+indent+"}")
        if nslots > 0:
            outfile.write("\n")
