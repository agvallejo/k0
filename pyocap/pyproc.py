import os
from pyutils import *
from pyocap  import *
from pygpt   import *
from pypage  import *
from elftools.elf.elffile import ELFFile
from elftools.elf.constants import P_FLAGS

# Memory segment
class Memseg:
    def __init__(self, data : bytes, base: int, zeros: int, restr: int):
        """
        Initialises a segment. The payload in 'data' is expanded by 'zeros',
        and 'base' and segment size are aligned (adding zeros at the head and
        tail of 'data'). The result is that zeros are added to data before
        and after 'data' in order to end up with a page aligned segment

        Parameters
        ----------
        data : bytes
            Octet stream with the payload contained in the segment
        base : int
            Virtual start address of the segment
        zeros : int
            Number of zeros to be added to 'data' for the segment to be complete
        """
        self.base  = base
        self.zeros = zeros
        self.restr = restr
        self.data  = data+bytes(zeros*[0])
        if not page_aligned(base):
            extra = base & PAGE_MASK
            self.data = bytes(extra*[0])+self.data
            self.base -= extra
        if not page_aligned(len(self.data)):
            extra = PAGE_SIZE - (PAGE_MASK & len(self.data))
            self.data += bytes(extra*[0])
        if (not page_aligned(self.base)) or (not page_aligned(len(self.data))):
            print(f"base={hex(base)} len={hex(len(data))}")
            print(f"aligned_base={hex(self.base)} aligned_len={hex(len(self.data))}")
            raise Exception("Unaligned segment")

    def start(self) -> int:
        """ Returns the virtual address of the first page of the segment """
        return self.base

    def size(self) -> int:
        """ Returns the size of the segment, including page-overheads """
        return len(self.data)

    def end(self) -> int:
        """ Returns the virtual address of the first page after the segment """
        return self.start()+self.size()

    def contains(self, start: int) -> bool:
        """
        Returns True if 'start' is contained in this segment, else False

        Parameters
        ----------
        start : int
            Address to check
        """
        return self.start() <= start and start < self.end()

    def disjoint(self, start:int, end: int) -> bool:
        """
        Returns True if the region defined by (start, end)
        is disjoint from this segment, else False

        Parameters
        ----------
        start : int
            Address to process
        end : int
            Address to process
        """
        return end <= self.start() or start >= self.end()

    def slice(self, start: int, end: int):
        """
        Returns None if the region defined by (start, end) is not contained in this segment
        Returns a new Segment if there is some overlap, the new Segment defines the overlapped area

        Parameters
        ----------
        start : int
            Address to process
        end : int
            Address to process
        """
        if self.disjoint(start, end):
            return None

        data = self.data

        if self.contains(start):
            data = data[start-self.start():]
        else:
            start = self.start()

        if self.contains(end):
            data = data[:end-self.end()]
        else:
            end = self.end()

        return Memseg(data, start, 0, self.restr)

class Segset:
    def __init__(self):
        """ Initialises a Segset as an empty list of Segments """
        self.segs = []

    def count(self) -> int:
        """ Returns the number of segments in the Segseg """
        return len(self.segs)

    def add(self, seg: Memseg):
        """
        Adds a Segment to the Segset

        :param seg: Segment to add. Must be located after all other contained segments
        :raises Exception: Segment wasn't located after the last segment in the set
        """
        if seg is None:
            return
        if (self.count()>0) and (seg.start() < self.segs[-1].end()):
            raise Exception("Invalid segment")
        self.segs.append(seg)

    def slice(self, start: int, size: int):
        """
        Slices a Segset creating a new Seg that only
        contains segments from the region (start, size)

        Parameters
        ----------
        :param start: Starting address of the new Segset
        :param size:  Span of the new Segset
        """
        segset = Segset()
        for seg in self.segs:
            segset.add(seg.slice(start, start+size))
        return segset

    def gen_tree(self, bank):
        """
        Slices a Segset creating a new Seg that only
        contains segments from the region (start, size)

        :param start: Starting address of the new Segset
        :param size: Span of the new Segset
        """
        # Case 1: No tree if no segments
        if not self.count():
            return None

        # Case 2: If all we have is a page, return that. It's virtual address is its guard
        if (self.count() == 1) and (self.segs[0].size() == PAGE_SIZE):
            page = bank.page(self.segs[0].data)
            return page.cap(self.segs[0].start())

        # Case 3: We have several segments. Find first and last pages
        start = self.segs[0].start()
        end = self.segs[-1].end()

        # Find the smallest 2**l2size region that contains this full segset
        guard, l2size = find_l2region(self.segs[0].start(), self.segs[-1].end())
        gpt = bank.gpt(l2size)

        for i in range(GPT_SIZE):
            segset = self.slice(guard+i*gpt.size_child, gpt.size_child)
            cap = segset.gen_tree(bank)
            if cap:
		# Retrieve object and create a new cap
                gpt.slot[i] = cap.obj.cap(cap.guard^guard)

        return gpt.cap(guard)

    def dump(self):
        """ Prints the contained segments in this set """
        print("---dump_start---")
        for seg in self.segs:
            print(f"   start={hex(seg.start())}")
            print(f"   end={hex(seg.end())}")
        print("---dump_end---")

class Process(Obj):
    def __init__(self, bank, filename, prefix, oid=None):
        super().__init__(oid)
        self.filename = os.path.join(prefix, filename)
        self.addr_space = self.__parse(bank, self.filename)
        self.regbank = PROC_NUM_REGS*[None]

    def __setitem__(self, regnum, cap):
        self.regbank[regnum] = cap

    def __getitem__(self, regnum):
        return self.regbank[regnum]

    def cap(self):
        return Cap(self, 'ct_Start')

    def __parse(self, bank, filename):
        self.file = open(filename, 'rb')
        elf = ELFFile(self.file)
        segset = Segset()
        for seg in elf.iter_segments():
            if seg['p_type'] == 'PT_LOAD':
                restr = cr_None
                if not (seg['p_flags'] & P_FLAGS.PF_X):
                    restr |= cr_NoExec
                if not (seg['p_flags'] & P_FLAGS.PF_W):
                    restr |= cr_Weak | cr_ReadOnly
                segset.add(Memseg(seg.data(), seg['p_vaddr'], seg['p_memsz']-seg['p_filesz'], restr))
        self.entry = elf['e_entry']
        return segset.gen_tree(bank)

    def dump(self, indent=""):
        print(indent+f"filename={self.filename}")
        for regnum, cap in enumerate(self.regbank):
            if cap:
                print(f"reg[{regnum}]={cap.oid}")
        indent += "    "
        self.addr_space.dump()

    def save(self, outfile, indent=""):
        outfile.write(indent+ ".state = ps_Running,\n")
        outfile.write(indent+f".rip = {hex(self.entry)},\n")

        outfile.write(indent+ ".regs = {\n")
        indent += '    '
        outfile.write(indent+ f".hdr.oid = {self.oid},\n")
        outfile.write(indent+ ".cap = {\n")
        indent += '    '

        nslots = 0
        for i, slot in enumerate(self.regbank):
            if slot:
                if nslots > 0:
                    outfile.write(",\n")
                nslots += 1
                outfile.write(indent+f"[{i}] = {{\n")
                slot.save(outfile, indent+4*' ')
                outfile.write(indent+"}")

        indent = indent[:-4]
        outfile.write("\n"+indent+"}")
        if nslots > 0:
            outfile.write("\n")

        indent=indent[:-4]
        outfile.write(indent+"}\n")

