#!/usr/bin/env python3
from pybank import *
from pyocap import *
import sys

ROOTFILE=sys.argv[1]
OUTDIR=sys.argv[2]

print("rootfile="+ROOTFILE)
print("outdir="+OUTDIR)

bank = Bank()

root1 = bank.proc(ROOTFILE)
root2 = bank.proc(ROOTFILE)

console = Cap(obj=None, type='ct_Console')
root1[0] = console;
root2[0] = console;

root1[15] = root2.cap();

bank.save(OUTDIR)
