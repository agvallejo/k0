from pyocap import *
from math import ceil

class Page(Obj):
    def __init__(self, data, oid=None):
        super().__init__(oid)
        self.data = data

    def cap(self, guard):
        return Cap(self, 'ct_Page', guard)

    def dump(self, indent=""):
        print(indent+f"pagelen={len(self.data)}")
        print(indent+f"data={self.data[:8]}...")

    def save(self, outfile, indent=""):
        nrows = ceil(len(self.data)/8)
        pos = 0
        end= len(self.data)
        for lineno in range(nrows):
            outfile.write(indent)
            outfile.write("0x{:02x}".format(self.data[pos]))
            pos += 1
            remaining = end-pos
            if end-pos>7:
                remaining = 7

            while remaining:
                outfile.write(", 0x{:02x}".format(self.data[pos]))
                remaining -= 1
                pos += 1
            if lineno < nrows-1:
                outfile.write(',')
            outfile.write("\n")
