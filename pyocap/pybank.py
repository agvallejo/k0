from pyproc import * 

class Bank:
    def __init__(self):
        self.procs = []
        self.gpts  = []
        self.pages = []

    def proc(self, filename, prefix='build/usr/'):
        oid = len(self.procs)
        proc = Process(self, filename, prefix, oid)
        self.procs.append(proc)
        return proc

    def gpt(self, l2size):
        oid = len(self.gpts)
        gpt = GPT(l2size, oid)
        self.gpts.append(gpt)
        return gpt

    def page(self, data):
        oid = len(self.pages)
        page = Page(data, oid)
        self.pages.append(page)
        return page

    def save(self, outdir):
        file='bank_preload'
        with open(os.path.join(outdir, file+'.c'), 'w+') as outfile:
            outfile.write('#include <k0/proc.h>\n')
            outfile.write('#include <k0/gpt.h>\n')
            outfile.write(f'#include "{file}.h"\n\n')

            outfile.write('proc_t bank_proc[] = {\n')
            indent = '    '
            for proc in self.procs:
                outfile.write(indent+f'[{proc.oid}] = '+'{\n')
                proc.save(outfile, indent=indent+'    ')
                outfile.write(indent+'}')
                if proc is not self.procs[-1]:
                    outfile.write(',')
                outfile.write('\n')
            outfile.write('};\n\n')

            outfile.write('gpt_t bank_gpt[] = {\n')
            indent = '    '
            for gpt in self.gpts:
                outfile.write(indent+f'[{gpt.oid}] = '+'{\n')
                gpt.save(outfile, indent=indent+'    ')
                outfile.write(indent+'}')
                if gpt is not self.gpts[-1]:
                    outfile.write(',')
                outfile.write('\n')
            outfile.write('};\n\n')

            outfile.write('page_t bank_page[] = {\n')
            indent = '    '
            for page in self.pages:
                outfile.write(indent+f'[{page.oid}] = '+'{\n')
                page.save(outfile, indent=indent+'    ')
                outfile.write(indent+'}')
                if page is not self.pages[-1]:
                    outfile.write(',')
                outfile.write('\n')
            outfile.write('};\n')

        with open(os.path.join(outdir, 'include', file+'.h'), 'w+') as outfile:
            outfile.write('#ifndef PYOCAP_BANK_PRELOAD_H\n')
            outfile.write('#define PYOCAP_BANK_PRELOAD_H\n\n')
            outfile.write('#include <k0/proc.h>\n')
            outfile.write('#include <k0/gpt.h>\n')
            outfile.write('\n')
            outfile.write(f'extern proc_t bank_proc[{self.procs[-1].oid+1}];\n')
            outfile.write('\n')
            outfile.write(f'extern gpt_t  bank_gpt[{self.gpts[-1].oid+1}];\n')
            outfile.write('\n')
            outfile.write('typedef uint8_t page_t[PAGE_SIZE];\n')
            outfile.write(f'extern page_t bank_page[{self.pages[-1].oid+1}];\n\n')
            outfile.write('#endif /* PYOCAP_BANK_PRELOAD_H */\n\n')
