DIR<pyocap>:=$(DIR)
BIN<pyocap>:=$(DIR)mkimage.py
OUTDIR<pyocap>:=$(BUILDDIR)pyocap/
INCLUDE<pyocap>:=$(OUTDIR<pyocap>)include/

SRCS_PY<pyocap>:=$(wildcard $(DIR)*.py)
# Export the .c files for consumption by the EFI loader
SRCS_C<pyocap>:=$(OUTDIR<pyocap>)bank_preload.c
SRCS_H<pyocap>:=$(OUTDIR<pyocap>)include/bank_preload.h

# FIXME: This is not quite right. Make triggers the rule twice (one per file),
#        which it should not. If it's going to do that it may be worth spliting
#        generation of .h and .c for purposes of build consistency.
$(SRCS_H<pyocap>) $(SRCS_C<pyocap>) &: $(SRCS_PY<pyocap>) $(BIN<usr/root>)
	mkdir -p $(OUTDIR<pyocap>) $(INCLUDE<pyocap>)
	$(BIN<pyocap>) $(CURDIR)/$(BIN<usr/root>) $(CURDIR)/$(OUTDIR<pyocap>)
