# Capability restrictions
cr_None     = 0
cr_Opaque   = 1<<0
cr_ReadOnly = 1<<1
cr_Weak     = 1<<2
cr_NoExec   = 1<<3

# Architecture bit manipulation constants
ARCH_BITS = 64
ARCH_SIZE = 1<<ARCH_BITS
ARCH_MASK = ARCH_SIZE

# GPT bit manipulation constants
GPT_BITS = 4
GPT_SIZE = 1<<GPT_BITS
GPT_MASK = GPT_SIZE-1

# Page bit manipulation constants
PAGE_BITS = 12
PAGE_SIZE = 1<<PAGE_BITS
PAGE_MASK = PAGE_SIZE-1

# Number of capability registers in a process
PROC_NUM_REGS = GPT_SIZE

# Helpful auxiliary functions
def page_aligned(addr):
    return False if (addr & PAGE_MASK) else True

def mask_hi(addr, bits):
    return addr & ~((1<<bits)-1)

def mask_lo(addr, bits):
    return addr & ((1<<bits)-1)

def find_l2region(start, end):
    for bit in range(PAGE_BITS, ARCH_BITS, GPT_BITS):
        guard = mask_hi(start, bit)
        if (guard + (1<<bit)) >= end:
            return guard, bit
    raise Exception("Error finding l2size")

