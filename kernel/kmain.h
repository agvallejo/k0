#ifndef K0_KMAIN_H
#define K0_KMAIN_H

/**
 * Entry point to the kernel from the bootloader
 *
 * @param hbt Handover Boot Table
 */
void kmain(struct hbt *hbt);

#endif /* K0_KMAIN_H */
