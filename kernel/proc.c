#include <k0/config.h>
#include <k0/percpu.h>

#include <k0/kassert.h>
#include <k0/proc.h>
#include <k0/cap.h>
#include <k0/proc.h>
#include <k0/msg.h>

cap_t *proc_fetchCap(proc_t *proc, capid_t capid, caprestr_t *restr)
{
	cap_t *cap = &proc->regs.cap[capid & GPT_MASK];
	if (capid & GPT_NUM_SLOTS)
		/* Indirect invocation. Walk a Node tree to find a leaf cap */
		return gpt_walk(cap, capid & ~PAGE_MASK, restr);

	/* Direct invocation. Use a cap in a register */
	kassert(capid, <, GPT_NUM_SLOTS);
	*restr = cr_None; /* No parent */
	return cap;
}

void proc_storeCap(proc_t *proc, capid_t capid, cap_t *capContent)
{
	caprestr_t restr = cr_None;
	cap_t *cap = proc_fetchCap(proc, capid, &restr);
	kassert_false(restr, &, cr_Weak|cr_ReadOnly);
	*cap = *capContent;
}

void proc_invokeCap(proc_t *proc, capid_t capid, msg_t *msg)
{
	caprestr_t restr = cr_None;
	cap_t *cap = proc_fetchCap(proc, capid, &restr);

	/* Opaque invocations are forbidden */
	kassert_false(cap->any.restr, &, cr_Opaque);

	/* Remote weak invocations are forbidden
	 *
	 * This is not such a big deal because the only weak capabilities worth
	 * invoking are GPT capabilities doing fetchSlot. GPT manipulation is
	 * subsumed by proc_copyCap, so it's not a big loss.
	 */
	kassert_false(restr, &, cr_Weak);

	cap_invoke(cap, msg);
}

void proc_copyCap(proc_t *proc, capid_t dst, capid_t src, caprestr_t new_restr)
{
	caprestr_t restr;

	restr = cr_None;
	cap_t *kdst = proc_fetchCap(proc, dst, &restr);
	kassert_false(restr, &, cr_Weak|cr_ReadOnly);

	restr = cr_None;
	cap_t *ksrc = proc_fetchCap(proc, src, &restr);

	/* If src is transitively weak, dst must be weak as well */
	new_restr |= restr & cr_Weak;
	cap_copy(kdst, ksrc, new_restr);
}

void proc_start(cap_t *cap, msg_t *msg)
{
	(void)msg;
	proc_t *proc = cap->proc.obj;
	kassert(proc->state, ==, ps_Available);

	proc->state = ps_Running;
	percpu(proc)->state = ps_Waiting;
	percpu(self)->proc = proc;

	/* FIXME: Cap grant */
}

void proc_resume(cap_t *cap, msg_t *msg)
{
	(void)msg;
	proc_t *proc = cap->proc.obj;
	kassert(proc->state, ==, ps_Waiting);

	proc->state = ps_Running;
	percpu(proc)->state = ps_Available;
	percpu(self)->proc = proc;

	/* FIXME: Cap grant */
}

void proc_mint(cap_t *targetCap, cap_t *smithCap, msg_t *msg)
{
	oid_t startOID = smithCap->smith.startOID;
	oid_t endOID   = smithCap->smith.endOID;

	/* The new OID is in-bounds */
	oid_t targetOID = msg->param2;
	kassert(targetOID, >=, startOID);
	kassert(targetOID, <, endOID);

	/* Request is valid. Go ahead */
	targetCap->proc.type = ct_Start;
	targetCap->proc.oid  = targetOID;

	/* FIXME: Uninitialised! Can't be until the process is prepared */
}
