#include <k0/percpu.h>
#include <k0/sys.h>

#include <k0/cap.h>
#include <k0/gpt.h>
#include <k0/proc.h>
#include <k0/msg.h>
#include <k0/cap.h>
#include <k0/kassert.h>

static uintptr_t mask_lo(uintptr_t addr,  uint8_t bits)
{
	/* Careful with undefined behavior... */
	uintptr_t mask = (2ull<<(bits-1))-1;
	return addr & mask;
}

static uintptr_t mask_hi(uintptr_t addr,  uint8_t bits)
{
	/* Careful with undefined behavior... */
	uintptr_t mask = (2ull<<(bits-1))-1;
	return addr & ~mask;
}

static void gpt_store(cap_t *cap, msg_t *msg)
{
	capid_t gptSlot = sys_gptStore_msg_getSlotNum(msg);

	caprestr_t restr;
	capreg_t reg = sys_gptStore_msg_getSrc(msg);
	cap_t *sourceCap = proc_fetchCap(percpu(proc), reg, &restr);
	kassert(gptSlot, <, GPT_NUM_SLOTS);

	/* Add extra restrictions required by the invocation */
	restr |= sys_gptStore_msg_getAccessType(msg);

	/* GPT cap is prepared */
	gpt_t *gpt = cap->gpt.obj;
	kassert(gpt, !=, NULL);

	gpt->cap[gptSlot] = *sourceCap;
}

static void gpt_fetch(cap_t *cap, msg_t *msg)
{
	capid_t procSlot = sys_gptFetch_msg_getDst(msg);
	kassert(procSlot, <, GPT_NUM_SLOTS);
	capid_t gptSlot = sys_gptFetch_msg_getSlotNum(msg);
	kassert(gptSlot, <, GPT_NUM_SLOTS);

	/* GPT cap is prepared */
	gpt_t *gpt = cap->gpt.obj;
	kassert(gpt, !=, NULL);

	cap_t *sourceCap = &gpt->cap[gptSlot];
	proc_storeCap(percpu(proc), procSlot, sourceCap);
}

static void gpt_update(cap_t *cap, msg_t *msg)
{
	uintptr_t guard = sys_gptUpdate_msg_getGuard(msg);
	cap->gpt.guard = guard;
}

void gpt_invoke(cap_t *cap, msg_t *msg)
{
	kassert_false(cap->gpt.restr, &, cr_Opaque);

	ocgpt_t opcode = msg->opcode;
	switch(opcode) {
		case oc_gptFetch:
			gpt_fetch(cap, msg);
			return;
		case oc_gptStore:
			gpt_store(cap, msg);
			return;
		case oc_gptUpdate:
			gpt_update(cap, msg);
			return;
	}

	panic_unreachable();
}

cap_t *gpt_walk(cap_t *cap, uintptr_t addr, caprestr_t *restr)
{
	uintptr_t guard = cap->gpt.guard;
	switch(cap->any.type) {
		case ct_GPT:
			/* This is the only non-trivial case, so we
			 * will break here and deal with it properly
			 */
			if (cap->any.restr & cr_Opaque)
				break;

			/* Non-translator step. This must be a leaf */
			kassert(guard, ==, addr);
			return cap;
		case ct_Page:
		case ct_Start:
		case ct_Resume:
		#ifdef TESTING
		case ct_Hello:
		#endif /* TEST */
		case ct_Smith:
		case ct_Loopback:
		case ct_Console:
		case ct_Invalid:
			kassert(addr, ==, 0);
			return cap;
	}

	/* Update parent's restrictions, but forward transitive restrictions */
	*restr = cap->any.restr | (*restr & cr_Weak);

	/* log2(slot size) */
	uint8_t slot_bitsize = cap->gpt.slot_bitsize;

	/* It's nonsensical to guard part of the spanned address space */
	kassert(mask_lo(guard, slot_bitsize), ==, 0);

	/* Basically anything in the address above slot_bitsize should match
	 * the guard. That way we can safely skip levels traversing GPTs
	 * The guard includes the index bit, so it gets cleared from addr
	 * as well along the guard itself
	 */
	kassert(mask_hi(addr, slot_bitsize), ==, guard);

	cap_prepare(cap);
	kassert(cap->gpt.obj, !=, NULL);

	/* From this point onwards 'cap' is a prepared cap */
	uint8_t gptSlot = addr>>(slot_bitsize-GPT_BITS);
	cap = &cap->gpt.obj->cap[gptSlot];

	addr ^= guard;
	/* We are recursing, BUT the recursive call is at the very end so the
	 * optimiser can easily convert this recursion into iteration. And it's
	 * far easier to read writing it recursively, so like this it shall stay
	 */
	return gpt_walk(cap, addr, restr);
}

/* External linkage, as it's triggered by invocation of Smith caps */
void gpt_mint(cap_t *targetCap, cap_t *smithCap, msg_t *msg)
{
	oid_t oid = msg->param2; /* OID of the new GPT */
	kassert(oid, >=, smithCap->smith.startOID);
	kassert(oid, <,  smithCap->smith.endOID);

	uint8_t slot_bitsize = msg->param3; /* Num. bits spanned by each slot */
	kassert(slot_bitsize, <=, 8*sizeof(uintptr_t));

	*targetCap = (cap_t){0};
	targetCap->gpt.type = ct_GPT;
	targetCap->gpt.oid  = oid;
	targetCap->gpt.slot_bitsize = slot_bitsize;
	targetCap->gpt.obj  = NULL; /* Unprepared */
}
