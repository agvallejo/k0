section .text
global _kmain
_kmain:
	; If the magic word is wrong we haven't been
	; properly bootstrapped and we don't have
	; a terminal or anything to whine about it
	cmp rdi, qword 0x19920413
	jne .hang

	; Setup a stack for the C environment
	extern kmain_stack
	mov rsp, kmain_stack+16384
	xor rbp, rbp
	push rbp ; RIP=0
	push rbp ; RBP=0
	mov rbp, rsp

	mov rdi, rsi ; Second parameter (HBT) is the first now
	extern kmain
	call kmain
.hang:
	cli
	hlt
	jmp .hang
