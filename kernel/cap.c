#include <k0/log.h>
#include <k0/kassert.h>
#include <k0/cap.h>
#include <k0/smith.h>
#include <k0/gpt.h>
#include <k0/proc.h>
#include <k0/cache.h>

void cap_copy(cap_t *dst, cap_t *src, caprestr_t restr)
{
	*dst = *src;
	dst->any.restr |= restr;
}

void cap_prepare(cap_t *cap)
{
	switch(cap->any.type) {
		/* Object caps */
		case ct_GPT:
		case ct_Page:
		case ct_Start:
		case ct_Resume:
			break;
		/* Objectless caps */
		#ifdef TESTING
		case ct_Hello:
		#endif /* TEST */
		case ct_Smith:
		case ct_Loopback:
		case ct_Console:
		case ct_Invalid:
			return;
	}

	obj_t *obj = cap->obj.obj;
	if (!obj) {
		/* Load the cache */
		obj = cache_load(cap->obj.type, cap->obj.oid);
		cap->obj.obj = obj;
	}

	kassert((uintptr_t)obj, >, PAGE_SIZE);
	kassert(obj->oid, ==, cap->obj.oid);
}

void cap_invoke(cap_t *cap, msg_t *msg)
{
	captype_t type = cap->any.type;
	cap_prepare(cap);

	switch(type) {
		/* Object caps */
		case ct_GPT:
			gpt_invoke(cap, msg);
			return;
		case ct_Page:
			panic_unimplemented();
			break;
		case ct_Start:
			proc_start(cap, msg);
			return;
		case ct_Resume:
			proc_resume(cap, msg);
			return;

		/* Objectless caps */
		#ifdef TESTING
		case ct_Hello:
			puts("Hello World!");
			return;
		#endif /* TEST */
		case ct_Smith:
			smith_invoke(cap, msg);
			return;
		case ct_Loopback:
			return;
		case ct_Console:
			panic_unimplemented();
			break;

		/* Null cap */
		case ct_Invalid:
			panic_unreachable();
	}
	panic_unreachable();
}
