#include <k0/config.h>
#include <k0/percpu.h>
#include <k0/kassert.h>
#include <k0/proc.h>
#include <k0/cap.h>
#include <k0/msg.h>
#include <k0/gpt.h>
#include <k0/smith.h>

static void smith_mint(cap_t *targetCap, cap_t *sourceCap, msg_t *msg)
{
	oid_t sourceStartOID = sourceCap->smith.startOID;
	oid_t sourceEndOID   = sourceCap->smith.endOID;

	/* The new startOID is in-bounds */
	oid_t targetStartOID = msg->param2;
	kassert(targetStartOID, >=, sourceStartOID);
	kassert(targetStartOID, <,  sourceEndOID);

	/* The new endOID is in-bounds */
	oid_t targetEndOID = msg->param3;
	kassert(targetEndOID, >=, sourceStartOID);
	kassert(targetEndOID, <,  sourceEndOID);

	/* Request is valid. Go ahead */
	*targetCap = *sourceCap;
	targetCap->smith.startOID = targetStartOID;
	targetCap->smith.endOID = targetEndOID;
}

static void smith_create(cap_t *cap, msg_t *msg)
{
	caprestr_t restr;
	captype_t targetType = msg->param1;
	cap_t *targetCap   = proc_fetchCap(percpu(proc), msg->cap1, &restr);

	/* Assert validity */
	kassert(targetCap->any.type, ==, ct_Invalid);

	/* Target must have write-permissions */
	kassert_false(restr, &, cr_Opaque|cr_ReadOnly);

	/* NOTE: Every field starts being zero due to type being ct_Invalid */
	switch(targetType) {
		case ct_Invalid:
			panic_unreachable();
			break; /* Dead code */

		case ct_Smith:
			smith_mint(targetCap, cap, msg);
			return;
		case ct_GPT:
			gpt_mint(targetCap, cap, msg);
			return;
		case ct_Start:
		case ct_Resume:
			proc_mint(targetCap, cap, msg);
			return;
		case ct_Page:
			panic_unimplemented();
			return;
	#ifdef TESTING
		case ct_Hello:
			/* FALLTHROUGH */
	#endif /* TESTING */
		case ct_Loopback:
		case ct_Console:
			/* This caps simply need the type */
			targetCap->any.type = targetType;
			return;
	}
	panic_unreachable();
}

static void smith_destroy(cap_t *cap, msg_t *msg)
{
	(void)cap;
	(void)msg;
	panic_unimplemented();
}

void smith_invoke(cap_t *cap, msg_t *msg)
{
	ocsmith_t opcode = msg->opcode;
	switch(opcode) {
		case oc_smithCreate:
			smith_create(cap, msg);
			return;
		case oc_smithDestroy:
			smith_destroy(cap, msg);
			return;
	}
	panic_unreachable();
}
