#include <k0/config.h>
#include <k0/boot.h>
#include <k0/kassert.h>

#include "kmain.h"

/* Stack used by the BSP during boot */
uint8_t kmain_stack[STACK_SIZE] __attribute__((aligned(STACK_SIZE)));

void kmain(struct hbt *hbt)
{
	(void)hbt;
	while(1);
	panic_unreachable();
}
