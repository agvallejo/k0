#include <stddef.h>

#include <k0/config.h>
#include <k0/obj.h>
#include <k0/cap.h>
#include <k0/cache.h>
#include <k0/kassert.h>

STATIC cache_t cache;

static obj_t *cache_hashGPT(oid_t oid)
{
	/* It's a directly mapped cache. I don't want
	 * to implement an aging strategy just yet.
	 */
	return &cache.gpt[oid%CACHE_NUM_GPTS].hdr;
}

static obj_t *cache_hashProc(oid_t oid)
{
	/* It's a directly mapped cache. I don't want
	 * to implement an aging strategy just yet.
	 */
	return &cache.proc[oid%CACHE_NUM_PROCESSES].regs.hdr;
}

static obj_t *cache_hashPage(oid_t oid)
{
	/* It's a directly mapped cache. I don't want
	 * to implement an aging strategy just yet.
	 */
	return &cache.page[oid%CACHE_NUM_PAGES];
}

void cache_init(void)
{
	/* FUN COMPILER BEHAVIOR: Present at least in clang10 and gcc9
	 *
	 * Description:
	 *     Seems related to the indecent size of cache_t.
	 *     They both try to use the stack to zero it out,
	 *     but because cache_t is massive it doesn't fit and it
	 *     segfaults. That's not fantastic. Not at all
	 *
	 * The code as it was:
	 *     cache = (cache_t){0};
	 *
	 * Conditions:
	 *     The flag -O0 needs to be set (I noticed while doing
	 *     some gdb eye-burning). Even -O1 is enough to fix it.
	 *
	 * Fix:
	 *     Using memset works, but I don't want libc in the kernel
	 *     so I'll simply use the builtin. That seems to work and
	 *     it's probably the optimised generated code ANYWAY.
	 */
	__builtin_memset(&cache, 0, sizeof cache);
}

void *cache_load(captype_t type, oid_t oid)
{
	obj_t *obj = NULL;
	switch(type) {
		/* Object caps */
		case ct_GPT:
			obj = cache_hashGPT(oid);
			break;
		case ct_Start:
		case ct_Resume:
			obj = cache_hashProc(oid);
			break;
		case ct_Page:
			obj = cache_hashPage(oid);
			break;

		/* Null and objectless caps */
		#ifdef TESTING
		case ct_Hello:
		#endif /* TEST */
		case ct_Smith:
		case ct_Loopback:
		case ct_Console:
		case ct_Invalid:
			break;
	}

	kassert_with_arg(obj && !obj->used, oid);
	obj->used = 1;
	obj->oid  = oid;
	return obj;
}
