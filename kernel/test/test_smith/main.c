#include <k0/kassert.h>
#include <k0/percpu.h>
#include <k0/gpt.h>
#include <k0/msg.h>
#include <k0/proc.h>
#include <k0/smith.h>

#include "test_harness.h"

#include <stdlib.h>

/* Dummy macros to annotate parameters */
#define IDENTITY(x)   (x ## u)
#define KEYID_SLOT(x) IDENTITY(x)
#define OID(x)        IDENTITY(x)
#define LSPACE(x)     IDENTITY(x)

/* Retrieve from the current process a cap_t* slot given a slot id */
#define KEY_SLOT(x) (&percpu(proc)->regs.cap[(x)])

static void* test_setup(const MunitParameter params[], void* user_data) {
	(void) params;
	test_fake_setup();
	return user_data;
}

static void test_teardown(void* fixture)
{
	(void)fixture;
	test_fake_teardown();
}

static MunitResult test_mintSmith(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	proc_t *proc = percpu(proc);

	/* Slots 0 and 1 are invalid */
	kassert(KEY_SLOT(0)->any.type, ==, ct_Invalid);
	kassert(KEY_SLOT(1)->any.type, ==, ct_Invalid);

	/* Create a smithCap for testing */
	cap_t cap = {0};
	cap.any.type = ct_Smith;
	cap.smith.startOID = OID(0x100);
	cap.smith.endOID   = OID(0x200);
	proc_storeCap(proc, KEYID_SLOT(0), &cap);

	/* Slot0 has a smith cap now */
	kassert(KEY_SLOT(0)->any.type, ==, ct_Smith);
	kassert(KEY_SLOT(0)->smith.startOID, ==, OID(0x100));
	kassert(KEY_SLOT(0)->smith.endOID,   ==, OID(0x200));

	/* Create a smith cap on slot1 from smith cap on slot0 */
	msg_t msg = {0};
	msg.opcode = oc_smithCreate;
	msg.param1 = ct_Smith;
	msg.param2 = cap.smith.startOID + 0x10;
	msg.param3 = cap.smith.endOID   - 0x10;
	msg.cap1   = KEYID_SLOT(1);
	proc_invokeCap(proc, KEYID_SLOT(0), &msg);

	/* Slot1 has a smith cap now */
	kassert(KEY_SLOT(1)->smith.type, ==, ct_Smith);
	kassert(KEY_SLOT(1)->smith.startOID, ==, 0x110);
	kassert(KEY_SLOT(1)->smith.endOID,   ==, 0x1F0);

	return MUNIT_OK;
}

static MunitResult test_mintHello(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	proc_t *proc = percpu(proc);

	cap_t cap = {0};
	cap.any.type = ct_Smith;
	cap.smith.startOID = OID(0x100);
	cap.smith.endOID   = OID(0x200);
	proc_storeCap(proc, KEYID_SLOT(0), &cap);

	msg_t msg;
	/* Create a smith cap on slot1 from smith cap on slot0 */
	msg = (msg_t){0};
	msg.opcode = oc_smithCreate;
	msg.param1 = ct_Smith;
	msg.param2 = cap.smith.startOID + 0x10;
	msg.param3 = cap.smith.endOID   - 0x10;
	msg.cap1   = KEYID_SLOT(1);
	proc_invokeCap(proc, KEYID_SLOT(0), &msg);

	/* Create a hello cap on slot14 from smith cap on slot1 */
	msg = (msg_t){0};
	msg.opcode = oc_smithCreate;
	msg.param1 = ct_Hello;
	msg.cap1   = KEYID_SLOT(14);
	proc_invokeCap(proc, KEYID_SLOT(1), &msg);

	/* Invoke hello world */
	proc_invokeCap(proc, KEYID_SLOT(14), &msg);

	return MUNIT_OK;
}
static MunitResult test_mintGPT(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	proc_t *proc = percpu(proc);

	cap_t cap = {0};
	cap.any.type = ct_Smith;
	cap.smith.startOID = OID(0x100);
	cap.smith.endOID   = OID(0x200);
	proc_storeCap(proc, KEYID_SLOT(0), &cap);

	msg_t msg;
	/* Create a GPT cap on slot1 from smith cap on slot0 */
	msg = (msg_t){0};
	msg.opcode = oc_smithCreate;
	msg.param1 = ct_GPT;
	msg.param2 = OID(0x120);
	msg.param3 = LSPACE(36);
	msg.cap1   = KEYID_SLOT(1);
	proc_invokeCap(proc, KEYID_SLOT(0), &msg);

	/* Slot1 has a GPT cap now */
	kassert(KEY_SLOT(1)->gpt.type, ==, ct_GPT);
	kassert(KEY_SLOT(1)->gpt.oid, ==, OID(0x120));

	return MUNIT_OK;
}

static MunitResult test_mintDomain(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	proc_t *proc = percpu(proc);

	cap_t cap = {0};
	cap.any.type = ct_Smith;
	cap.smith.startOID = OID(0x100);
	cap.smith.endOID   = OID(0x200);
	proc_storeCap(proc, KEYID_SLOT(0), &cap);

	msg_t msg;
	/* Create a GPT cap on slot1 from smith cap on slot0 */
	msg = (msg_t){0};
	msg.opcode = oc_smithCreate;
	msg.param1 = ct_Start;
	msg.param2 = OID(0x120);
	msg.cap1   = KEYID_SLOT(1);
	proc_invokeCap(proc, KEYID_SLOT(0), &msg);

	/* Slot1 has a GPT cap now */
	kassert(KEY_SLOT(1)->gpt.type, ==, ct_Start);
	kassert(KEY_SLOT(1)->gpt.oid, ==, OID(0x120));

	return MUNIT_OK;
}

static MunitTest test_suite_tests[] = {
	{ "/mint/hello",   test_mintHello,  test_setup, test_teardown, MUNIT_TEST_OPTION_NONE, NULL },
	{ "/mint/smith",   test_mintSmith,  test_setup, test_teardown, MUNIT_TEST_OPTION_NONE, NULL },
	{ "/mint/gpt",     test_mintGPT,    test_setup, test_teardown, MUNIT_TEST_OPTION_NONE, NULL },
	{ "/mint/process", test_mintDomain, test_setup, test_teardown, MUNIT_TEST_OPTION_NONE, NULL },
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite =
	{"/smith",  test_suite_tests,  NULL, 1, MUNIT_SUITE_OPTION_NONE};

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)]) {
  return munit_suite_main(&test_suite, NULL, argc, argv);
}
