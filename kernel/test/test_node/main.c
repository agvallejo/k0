#include <k0/kassert.h>
#include <k0/percpu.h>
#include <k0/gpt.h>
#include <k0/msg.h>
#include <k0/proc.h>
#include <k0/smith.h>
#include <k0/sys.h>

#include "test_harness.h"

#include <stdlib.h>

/* Dummy macros to annotate parameters */
#define KEYID_SLOT(x)   (x)
#define OID(x)          (x)
#define SLOT_BITSIZE(x) (x)
#define GUARD(x)        (x)
#define ADDR(x)         (x)
#define RESTR(x)        (x)

/* Retrieve from the current process a cap_t* slot given a slot id */
#define KEY_SLOT(x) (&percpu(proc)->regs.cap[(x)])

static void* test_setup(const MunitParameter params[], void* user_data) {
	(void) params;
	test_fake_setup();
	return user_data;
}

static void test_teardown(void* fixture)
{
	(void)fixture;
	test_fake_teardown();
}

static MunitResult test_gptStoreFetch(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	proc_t *proc = percpu(proc);

	/* ct_Smith -> reg[0] */
	cap_t cap = {0};
	cap.any.type = ct_Smith;
	cap.smith.startOID = OID(0x100);
	cap.smith.endOID   = OID(0x200);
	proc_storeCap(proc, KEYID_SLOT(0), &cap);

	/* reg[0].create(ct_GPT) -> reg[1] */
	sys_smithCreate(KEYID_SLOT(0), ct_GPT, OID(0x120), SLOT_BITSIZE(21), KEYID_SLOT(1));

	/* type(reg[1]) == ct_GPT */
	kassert(KEY_SLOT(1)->gpt.type,   ==, ct_GPT);
	kassert(KEY_SLOT(1)->gpt.oid,    ==, OID(0x120));
	kassert(KEY_SLOT(1)->gpt.slot_bitsize, ==, SLOT_BITSIZE(21));

	/* reg[0] -> reg[1].slot[5] */
	sys_gptStore(KEYID_SLOT(1), KEYID_SLOT(5), KEYID_SLOT(0), cr_None);

	/* type(reg[1].slot[5]) == ct_Smith */
	cap_t *cap1 = KEY_SLOT(1);
	kassert(cap1->gpt.obj->cap[KEYID_SLOT(5)].smith.type, ==, ct_Smith);

	/* reg[1].slot[5] -> reg[12] */
	sys_gptFetch(KEYID_SLOT(1), KEYID_SLOT(5), KEYID_SLOT(12));

	/* type(reg[12]) == ct_Smith */
	cap_t *cap12 = KEY_SLOT(12);
	kassert(cap12->smith.type, ==, ct_Smith);

	return MUNIT_OK;
}

static MunitResult test_gptWalk(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	proc_t *proc = percpu(proc);

	/* Create a smith cap on slot0 */
	cap_t cap = {0};
	cap.any.type = ct_Smith;
	cap.smith.startOID = OID(0x100);
	cap.smith.endOID   = OID(0x200);
	proc_storeCap(proc, KEYID_SLOT(0), &cap);

	/* reg[0].create(ct_GPT) -> reg[1] */
	sys_smithCreate(KEYID_SLOT(0), ct_GPT, OID(0x120), SLOT_BITSIZE(64), KEYID_SLOT(1));

	/* reg[0].create(ct_GPT) -> reg[2] */
	sys_smithCreate(KEYID_SLOT(0), ct_GPT, OID(0x121), SLOT_BITSIZE(12), KEYID_SLOT(2));

	/* reg[2].update() */
	sys_gptUpdate(KEYID_SLOT(2), GUARD(0xFFFFFFFF12345000ull));

	/* reg[2] -> reg[1].slot[31] */
	sys_gptStore(KEYID_SLOT(1), KEYID_SLOT(31), KEYID_SLOT(2), cr_None);

	/* Copy a downgraded version of the GPT in slot1 to itself (so we can walk it) */
	proc_copyCap(proc, KEYID_SLOT(1), KEYID_SLOT(1), cr_Opaque);

	/* reg[1].walk() -> reg[3] */
	proc_copyCap(proc, KEYID_SLOT(3), ADDR(0xFFFFFFFF12345000ull) | GPT_NUM_SLOTS | KEYID_SLOT(1), cr_None);

	/* Walk the GPT cap from slot1 to retrieve into slot3 the guarded GPT in slot31 */
	/* Same GPT if they have the same OID */
	cap_t *cap3 = KEY_SLOT(3);
	cap_t *cap2 = KEY_SLOT(2);
	kassert(cap3->gpt.oid, ==, cap2->gpt.oid);

	return MUNIT_OK;
}

static MunitTest test_suite_tests[] = {
	{ "/store_fetch", test_gptStoreFetch, test_setup, test_teardown, MUNIT_TEST_OPTION_NONE, NULL },
	{ "/walk",        test_gptWalk,       test_setup, test_teardown, MUNIT_TEST_OPTION_NONE, NULL },
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite =
	{"/gpt",  test_suite_tests,  NULL, 1, MUNIT_SUITE_OPTION_NONE};

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)]) {
  return munit_suite_main(&test_suite, NULL, argc, argv);
}
