#include <k0/kassert.h>
#include <k0/percpu.h>
#include <k0/gpt.h>
#include <k0/msg.h>
#include <k0/proc.h>
#include <k0/smith.h>
#include <k0/sys.h>

#include "test_harness.h"

#include <stdlib.h>

/* Dummy macros to annotate parameters */
#define KEYID_SLOT(x)   (x)
#define OID(x)          (x)
#define SLOT_BITSIZE(x) (x)
#define GUARD(x)        (x)
#define ADDR(x)         (x)
#define RESTR(x)        (x)

/* Retrieve from the current process a cap_t* slot given a slot id */
#define KEY_SLOT(x) (&percpu(proc)->regs.cap[(x)])

static void* test_setup(const MunitParameter params[], void* user_data) {
	(void) params;
	test_fake_setup();
	return user_data;
}

static void test_teardown(void* fixture)
{
	(void)fixture;
	test_fake_teardown();
}

static MunitResult test_procStart(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	proc_t *proc = percpu(proc);

	/* ct_Smith -> reg[0] */
	cap_t cap = {0};
	cap.any.type = ct_Smith;
	cap.smith.startOID = OID(0x100);
	cap.smith.endOID   = OID(0x200);
	proc_storeCap(proc, KEYID_SLOT(0), &cap);

	/* reg[0].create(ct_Start) -> reg[1] */
	//sys_smithCreate(KEYID_SLOT(0), ct_Start, OID(0x120), 0, 0);

	/* reg[0] -> reg[1].slot[5] */
	//sys_procStart(KEYID_SLOT(1), KEYID_SLOT(5), KEYID_SLOT(0), cr_None);

	return MUNIT_OK;
}

static MunitTest test_suite_tests[] = {
	{ "/start", test_procStart, test_setup, test_teardown, MUNIT_TEST_OPTION_NONE, NULL },
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite =
	{"/proc",  test_suite_tests,  NULL, 1, MUNIT_SUITE_OPTION_NONE};

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)]) {
  return munit_suite_main(&test_suite, NULL, argc, argv);
}
