ENV:=munit
$(eval $(call add_to_testlist,$(ENV)))

SRCS_C<$(TEST)>:=$(DIR)main.c \
                 $(DIR<kernel>)cap.c \
                 $(DIR<kernel>)cache.c \
                 $(DIR<kernel>)gpt.c \
                 $(DIR<kernel>)smith.c \
                 $(DIR<kernel>)proc.c \
                 $(DIR<munit>)munit.c
CPPFLAGS<$(TEST)>+=-I$(INCLUDE<kernel>)

SRCS_C<$(TEST)>+=$(SRCS_C<test_harness>)
CPPFLAGS<$(TEST)>+=$(CPPFLAGS<test_harness>)

$(eval $(call spawn_c_rule,$(TEST)))
$(eval $(call spawn_c_link_rule,$(TEST)))
