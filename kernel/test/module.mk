SRCS_C<test_harness>:=$(DIR)test_harness.c
CPPFLAGS<test_harness>:=-I$(DIR)

# Include all tests
$(eval $(call include_mkfiles,$(wildcard $(DIR)test_*/module.mk)))
