#include <k0/percpu.h>
#include <k0/proc.h>
#include <k0/cache.h>
#include <k0/kassert.h>
#include <stdlib.h>

#include "test_harness.h"

percpu_t *test_percpu;

void test_fake_setup(void)
{
	percpu_t *p = calloc(1, sizeof *p);
	p->self = p;
	p->coreid = 3; /* A non-zero CPU, for kicks */

	percpu_set(p);
	kassert(p->coreid, ==, 3);
	kassert(p, ==, percpu(self));

	cache_init();
	p->proc = cache_load(ct_Start, 0); /* initialise proc0 */
}

void test_fake_teardown(void)
{
	free(test_percpu);
}
