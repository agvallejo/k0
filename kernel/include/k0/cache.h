#ifndef K0_CACHE_H
#define K0_CACHE_H

#include <k0/config.h>
#include <k0/gpt.h>
#include <k0/proc.h>

typedef struct cache_t {
	gpt_t  gpt[CACHE_NUM_GPTS];
	proc_t proc[CACHE_NUM_PROCESSES];
	obj_t  page[CACHE_NUM_PAGES];
} cache_t;

/**
 * Initialise the cache singleton
 *
 * Wipe and init to zeros
 */
void cache_init(void);

/**
 * Load a cache slot with a specific object
 *
 * -Non-persistent objects are guaranteed to succeed being loaded
 * -Object types are deduced from capability types
 * -Objectless capabilities don't affect the cache
 *
 * @param type Capability type
 * @param type Capability type
 */
void *cache_load(captype_t type, oid_t oid);

#endif /* K0_CACHE_H */
