#ifndef K0_GPT_H
#define K0_GPT_H

#include <k0/cap.h>
#include <k0/obj.h>
#include <k0/config.h>

#include <stdbool.h>

typedef enum ocgpt_t {
	oc_gptFetch  = 1,
	oc_gptStore  = 2,
	oc_gptUpdate = 3,
} ocgpt_t;

typedef struct gpt_t {
	obj_t hdr;
	cap_t cap[GPT_NUM_SLOTS]; /**< Set of caps contained in the GPT */
} gpt_t;

/**
 * Invoke a GPT
 *
 * @param cap Capability to the GPT being invoked
 * @param msg Message for the GPT being invoked
 */
void gpt_invoke(cap_t *cap, msg_t *msg);

/**
 * Create a new GPT in 'targetCap' using 'smithCap'
 *
 * @param targetCap Target slot
 * @param smithCap Capability granting the authority to create a capability
 * @param msg Message intended for the smith capability 
 */
void gpt_mint(cap_t *targetCap, cap_t *smithCap, msg_t *msg);

/**
 * Find a capability traversing trees
 *
 * @param cap Root capability of the GPT
 * @param addr Address to walk with
 * @param restr[out] OR'ed restr fields of the visited capabilities
 */
cap_t *gpt_walk(cap_t *cap, uintptr_t addr, caprestr_t *restr);

#endif /* K0_GPT_H */
