#ifndef K0_PROC_H
#define K0_PROC_H

#include <k0/config.h>
#include <k0/gpt.h>

typedef struct gpt_t gpt_t;
typedef enum {
	ps_Available = 0, /**< Open-wait   */
	ps_Waiting   = 1, /**< Closed-wait */
	ps_Running   = 2  /**< Blocked     */
} procstate_t;

typedef struct proc_t {
	gpt_t regs;        /**< Implicit GPT (including obj_t header) */
	procstate_t state; /**< State of the process */
	uintptr_t rip;     /**< Entry point */
	uint8_t capidx[4]; /**< Indices into 'regs' for IPC */
} proc_t;

void proc_copyCap(proc_t *proc, capid_t dst, capid_t src, caprestr_t restr);
void proc_invokeCap(proc_t *proc, capid_t capid, msg_t *msg);
void proc_storeCap(proc_t *proc, capid_t capid, cap_t *cap);
cap_t *proc_fetchCap(proc_t *proc, capid_t capid, caprestr_t *restr);
proc_t *proc_getCurrent(void);
void proc_start(cap_t *cap, msg_t *msg);
void proc_resume(cap_t *cap, msg_t *msg);
void proc_mint(cap_t *targetCap, cap_t *smithCap, msg_t *msg);

#endif /* K0_PROC_H */
