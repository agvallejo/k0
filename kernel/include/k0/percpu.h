#ifndef K0_PERCPU_H
#define K0_PERCPU_H

#include <stdint.h>
#include <stddef.h>
#include <k0/hal.h>

typedef uintptr_t coreid_t;
typedef struct proc_t proc_t;
typedef struct percpu_t
{
	/* ALL ELEMENTS MUST BE UINTPTR_T SIZED (See percpu()) */
	void *rsp; /**< CPU-specific stack */
	struct percpu_t *self; /**< A pointer to itself */
	coreid_t coreid; /**< Core-id. Not normally needed, but available */
	proc_t *proc; /**< Domain currently executing */
} percpu_t;

#ifdef TESTING
extern percpu_t *test_percpu;
#endif /* TESTING */

/**
 * Establish 'data' as the percpu structure for this parti
 *
 * @param data A pointer to a suitable percpu structure with indefinite lifetime
 */
static inline void percpu_set(percpu_t *data)
{
#ifdef TESTING
	test_percpu = data;
#else /* TESTING */
	#define KernelGSBase (0xC0000102)
	wrmsr(KernelGSBase, (uintptr_t)data);
	swapgs();
#endif /* TESTING */
}

/**
 * Retrieve the absolute address of an offset into the percpu struct
 *
 * @param offset Offset into the percpu struct
 * @return Absolute address of the offset into the percpu struct
 */
static inline uintptr_t percpu_get(size_t offset)
{
	uintptr_t out;
#ifdef TESTING
	/* This is literally what the assembly does under the hood */
	out = *(uintptr_t*)((uintptr_t)test_percpu+offset);
#else /* TESTING */
	__asm__ volatile ("movq %%gs:%0, %1" : "=g"(out) : "r"(offset));
#endif /* TESTING */
	return out;
}

/**
 * Find the offset of a percpu field into the percpu struct
 *
 * @param field Name of the field
 * @return Offset into the percpu struct
 */
#define percpu_offsetof(field) offsetof(percpu_t, field)

/**
 * Find the type of a field of the percpu struct
 *
 * @param field Name of the field
 * @return "type" of the field
 */
#define percpu_typeof(field) __typeof__(((percpu_t*)0)->field)

/**
 * Retrieve a suitably casted field of the percpu struct
 *
 * Due to some assumptions in the macros leading to this, the pulled field
 * must be uintptr_t-sized and that's why all member must be pointer sized
 *
 * @param field Name of the field
 * @return Content of the field
 */
#define percpu(field) ((percpu_typeof(field))percpu_get(percpu_offsetof(field)))

#endif /* K0_PERCPU_H */
