#ifndef K0_SMITH_H
#define K0_SMITH_H

#include <k0/config.h>

typedef enum ocsmith_t{
	oc_smithCreate  = 1,
	oc_smithDestroy = 2
} ocsmith_t;

/**
 * Invoke a smith cap
 *
 * Target smith range cannot cover OIDs not covered by the source
 *
 * @param targetKey Slot to hold the new smith cap
 * @param sourceKey Slot holding the old smith cap
 * @param msg       Invocation message with range parameters
 */
void smith_invoke(cap_t *cap,  msg_t *msg);

#endif /* K0_SMITH_H */
