#ifndef K0_SYS_H
#define K0_SYS_H

#include <k0/proc.h>
#include <k0/smith.h>
#include <k0/msg.h>

#define GUARD_NONE (u)

static inline void sys_gptStore(capreg_t gpt, uint8_t slotnum,
                                 capreg_t src, uintptr_t access_type)
{
	msg_t msg;
	msg.opcode=oc_gptStore;
	msg.cap1=slotnum;
	msg.cap2=src;
	msg.param1=access_type;
	proc_invokeCap(percpu(proc), gpt, &msg);
}

static inline uintptr_t sys_gptStore_msg_getAccessType(const msg_t *msg)
{
	return msg->param1;
}

static inline uint8_t sys_gptStore_msg_getSlotNum(const msg_t *msg)
{
	return msg->cap1;
}

static inline capreg_t sys_gptStore_msg_getSrc(const msg_t *msg)
{
	return msg->cap2;
}

static inline void sys_gptFetch(capreg_t gpt, uint8_t slotnum, capreg_t dst)
{
	msg_t msg;
	msg.opcode=oc_gptFetch;
	msg.cap1=slotnum;
	msg.cap2=dst;
	proc_invokeCap(percpu(proc), gpt, &msg);
}

static inline uint8_t sys_gptFetch_msg_getSlotNum(const msg_t *msg)
{
	return msg->cap1;
}

static inline capreg_t sys_gptFetch_msg_getDst(const msg_t *msg)
{
	return msg->cap2;
}

static inline void sys_gptUpdate(capreg_t gpt, uintptr_t guard)
{
	msg_t msg;
	msg.opcode=oc_gptUpdate;
	msg.param1=guard;
	proc_invokeCap(percpu(proc), gpt, &msg);
}

static inline uintptr_t sys_gptUpdate_msg_getGuard(const msg_t *msg)
{
	return msg->param1;
}

static inline void sys_smithCreate(capreg_t smith, captype_t type, oid_t oid,
                                   uintptr_t slot_bitsize, capreg_t dst)
{
	msg_t msg;
	msg.opcode = oc_smithCreate;
	msg.param1 = type;
	msg.param2 = oid;
	msg.param3 = slot_bitsize;
	msg.cap1   = dst;
	proc_invokeCap(percpu(proc), smith, &msg);
}

#endif /* K0_SYS_H */

