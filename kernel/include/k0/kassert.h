#ifndef K0_KASSERT_H
#define K0_KASSERT_H

#define STATIC_ASSERT(...) _Static_assert(__VA_ARGS__, "Assertion failed")

#include <stdint.h>

void panic_simple(char *file, char *line, char *cond, uint64_t arg);
void panic_full(char *file, char *line, char* sarg1, char *op, char *sarg2, uint64_t arg1,  uint64_t arg2);

#define _STR(x) #x
#define _VAL(x) _STR(x)

#ifdef TESTING
#include <munit.h>
#undef PRIu64
#define PRIu64 "I64x"
#define kassert_true(arg1, op, arg2) munit_assert_type(munit_uint64_t, PRIx64, (uint64_t)(arg1), op, (uint64_t)(arg2))
#define kassert_false(arg1, op, arg2) munit_assert_false((uint64_t)(arg1) op (uint64_t)(arg2))
#define kassert_with_arg(cond, arg) munit_assert(cond)
#define panic(msg, arg) munit_error(msg)
#else /* TESTING */

#define kassert_true(arg1, op, arg2) \
       do{ \
               uint64_t _arg1 = (uint64_t)(arg1); \
               uint64_t _arg2 = (uint64_t)(arg2); \
               if (!((_arg1) op (_arg2))) { \
                       panic_full(__FILE__, _VAL(__LINE__), #arg1, #op, #arg2, _arg1, _arg2); \
               } \
       } while(0)

#define kassert_false(arg1, op, arg2) \
       do{ \
               uint64_t _arg1 = (uint64_t)(arg1); \
               uint64_t _arg2 = (uint64_t)(arg2); \
               if ((_arg1) op (_arg2)) { \
                       panic_full(__FILE__, _VAL(__LINE__), #arg1, #op, #arg2, _arg1, _arg2); \
               } \
       } while(0)

#define kassert_with_arg(cond, arg) ((cond)?(void)0:panic_simple(__FILE__, _VAL(__LINE__), #cond, (uint64_t)(arg)))
#define panic(msg, arg) panic_simple(__FILE__, _VAL(__LINE__), msg, (uint64_t)(arg))
#endif /* TESTING */

/* Convenience macros */
#define kassert(...) kassert_true(__VA_ARGS__)
#define kassert_notnull(ptr, arg) kassert_with_arg(ptr, arg)
#define kassert_bool(cond, arg) kassert_with_arg(cond, arg)

#define panic_unimplemented() panic("Kernel path not yet implemented", 0);
#define panic_unreachable() panic("Reached unreachable kernel path", 0);

#endif /* K0_KASSERT_H */
