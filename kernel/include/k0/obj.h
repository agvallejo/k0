#ifndef K0_OBJ_H
#define K0_OBJ_H

#include <k0/config.h>

#include <stdint.h>
#include <stdbool.h>

typedef struct obj_t {
	oid_t oid;
	uint64_t used     :  1; /** Unoccupied cache slot   */
	uint64_t reserved : 63; /** Reserved for future use */
} obj_t;

#endif /* K0_OBJ_H */
