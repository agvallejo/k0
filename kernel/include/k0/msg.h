#ifndef K0_MSG_H
#define K0_MSG_H

#include <stdint.h>
#include <k0/config.h>

typedef struct msg_t {
	uint8_t opcode;

	capid_t cap1;
	capid_t cap2;
	capid_t cap3;
	capid_t cap4;

	uint64_t param1;
	uint64_t param2;
	uint64_t param3;
	uint64_t param4;
} msg_t;

#endif /* K0_MSG_H */
