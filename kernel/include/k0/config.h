#ifndef K0_CONFIG_H
#define K0_CONFIG_H

#include <stdint.h>
#include <stddef.h>

/* Arch-specific definitions */
#define PAGE_BITS (12u)
#define PAGE_SIZE ((uintptr_t)(1<<(PAGE_BITS)))
#define PAGE_MASK ((PAGE_SIZE)-1)

#define STACK_BITS (12)
#define STACK_SIZE ((size_t)(1<<(STACK_BITS)))
#define STACK_MASK ((STACK_SIZE)-1)

#define CACHELINE_SIZE (64u)

/* Tunable parameters. These are pretty arbitrary */
#define GPT_BITS      (5u) /* log2(GPT_NUM_SLOTS) */
#define GPT_NUM_SLOTS ((uintptr_t)(1u<<GPT_BITS))
#define GPT_MASK      (GPT_NUM_SLOTS-1)

/* FIXME: This is for initial bootstraping. After K0 boots
 *        it should deduce the cache dynamically from the
 *        available memory map 
 */
#define CACHE_DEFAULT_ENTRIES (16u)
#define CACHE_NUM_GPTS        (CACHE_DEFAULT_ENTRIES)
#define CACHE_NUM_PROCESSES   (CACHE_DEFAULT_ENTRIES)
#define CACHE_NUM_PAGES       (CACHE_DEFAULT_ENTRIES)

/* Primitive types */
typedef uint64_t oid_t;   /**< Object ID   */
typedef uintptr_t capid_t; /**< Key slot ID */

/* Forward declaration of global types */
typedef struct gpt_t  gpt_t;
typedef struct proc_t proc_t;
typedef struct msg_t  msg_t;
typedef struct cap_t  cap_t;

#endif /* K0_CONFIG_H */
