#ifndef K0_LOG_H
#define K0_LOG_H

#ifdef TESTING
#include <stdio.h>
#define kputs(...) puts(__VA_ARGS__)
#define kprintf(...) printf(__VA_ARGS__)
#endif

#endif /* K0_LOG_H */
