#ifndef K0_CAP_H
#define K0_CAP_H

#include <k0/config.h>
#include <k0/kassert.h>

typedef enum {
	/** Async send. Drop message if receiving process not ready */
	ci_Send   = 1,
	/** Blocking send, then receive a message from the other end */
	ci_Call   = 2,
	/**< Blocking send, then go to an open-wait */
	ci_Return = 3
} capinv_t;

typedef enum {
	cr_None     = 0u,    /**< No restrictions at all */
	cr_Opaque   = 1<<0u, /**< Cap used only as a translation step */
	cr_ReadOnly = 1<<1u, /**< Prevent state mutating invocations */
	cr_Weak     = 1<<2u, /**< Transitive version of cr_ReadOnly */
	cr_NoExec   = 1<<3u  /**< Prevent executable mappings */
} caprestr_t;

typedef enum {
	/* The invalid type exist to detect mess-ups early */
	ct_Invalid   = 0, /**< Invalid cap */

	/* Objectless caps (type:=(0x01..0x0F)) */
	ct_Smith    = 0x01, /**< Constructor/destructor of caps */

	/* Object caps (type:=(0x10..0x1F)) */
	ct_GPT       = 0x10, /**< A GPT     object */
	ct_Page      = 0x11, /**< A Page    object */
	ct_Start     = 0x12, /**< A Process object (Waiting->Running) */
	ct_Resume    = 0x13, /**< A Process object (Blocked->Running) */

	/* Debugging caps (type:=(0x20..0x2F)) */
	#ifdef TESTING
	ct_Hello    = 0x20, /**< Helloworld printf and loopback */
	#endif /* TESTING */
	ct_Loopback = 0x21, /**< An IPC loopback path */
	ct_Console  = 0x22  /**< A capability to a kernel-implemented console */
} captype_t;

/** Opaque identifier of a cap */
typedef uintptr_t capaddr_t;

/** Cap register identifier */
typedef uintptr_t capreg_t;

typedef struct cap_t {
	union {
		struct{
			captype_t  type;
			caprestr_t restr;
			uint64_t   reserved[4];
		} any;
		struct{
			captype_t  type;
			caprestr_t restr;
			uint64_t   reserved[2];
			oid_t     startOID;
			oid_t     endOID;
		} smith;
		struct{
			captype_t  type;
			caprestr_t restr;
			uint64_t   reserved[2];
			void      *obj;
			oid_t      oid;
		} obj;
		struct{
			captype_t  type;
			caprestr_t restr;
			uint64_t   reserved[2];
			proc_t    *obj;
			oid_t      oid;
		} proc;
		struct{
			captype_t  type;
			caprestr_t restr;
			uint64_t   guard;
			uint64_t   slot_bitsize;
			gpt_t     *obj;
			oid_t      oid;
		} gpt;
		struct{
			captype_t  type;
			caprestr_t restr;
			uint64_t   guard;
			uint64_t   slot_bitsize;
			void      *obj;
			oid_t      oid;
		} page;
	};
} cap_t;

#define cap_type(cap)  ((captype_t)((cap)->any.type))
#define cap_restr(cap) ((caprestr_t)((cap)->any.restr))

/* Assert that the union didn't insert padding anywhere */
STATIC_ASSERT(sizeof((cap_t){0}.any) == sizeof((cap_t){0}.smith));
STATIC_ASSERT(sizeof((cap_t){0}.any) == sizeof((cap_t){0}.obj));
STATIC_ASSERT(sizeof((cap_t){0}.any) == sizeof((cap_t){0}.proc));
STATIC_ASSERT(sizeof((cap_t){0}.any) == sizeof((cap_t){0}.gpt));

/**
 * Copy a capability from 'src' to 'dst'
 *
 * @param dst Destination location
 * @param src Source location
 */
void cap_copy(cap_t *dst, cap_t *src, caprestr_t restr);

/**
 * Invoke a capability
 *
 * @param cap Capability to invoke
 * @param msg Message to deliver
 */
void cap_invoke(cap_t *cap, msg_t *msg);

/**
 * Prepare an unprepared cap
 *
 * Objectless caps are always prepared
 *
 * @param cap Unprepared cap
 */
void cap_prepare(cap_t *cap);

#endif /* K0_CAP_H */
