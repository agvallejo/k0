DIR<kernel>:=$(DIR)
INCLUDE<kernel>:=$(DIR)include
BIN<kernel>:=$(BUILDDIR)kernel/k0.elf

LDSCRIPT<$(BIN<kernel>)>:=$(DIR)linker.ld
SRCS_C<$(BIN<kernel>)>:=$(wildcard $(DIR)*.c) $(SRCS_C<libc>)
SRCS_ASM<$(BIN<kernel>)>:=$(wildcard $(DIR)*.asm)

CC<$(BIN<kernel>)>:=$(CC<target/amd64>)
CFLAGS<$(BIN<kernel>)>:=$(CFLAGS<target/amd64>) \
    -ffreestanding \
    -mno-red-zone \
    -mno-sse \
    -mcmodel=kernel
CPPFLAGS<$(BIN<kernel>)>:=$(CPPFLAGS<target/amd64>) \
    -nostdlibinc \
    -I$(INCLUDE<kernel>) \
    -I$(INCLUDE<libc>) \
    -DIS_KERNEL=1
AS<$(BIN<kernel>)>:=$(AS<target/amd64>)
ASFLAGS<$(BIN<kernel>)>:=$(ASFLAGS<target/amd64>)
LDFLAGS<$(BIN<kernel>)>:=$(LDFLAGS<target/amd64>) \
    -Wl,-T$(LDSCRIPT<$(BIN<kernel>)>) \
    -Wl,-z -Wl,max-page-size=4096
LDLIBS<$(BIN<kernel>)>:=-nostdlib
$(eval $(call spawn_c_rule,$(BIN<kernel>)))
$(eval $(call spawn_asm_rule,$(BIN<kernel>)))
$(eval $(call spawn_c_link_rule,$(BIN<kernel>)))

$(eval $(call include,$(DIR)test/module.mk))

