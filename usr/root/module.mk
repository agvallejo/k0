#-------------------------------------------------------------------------------
# Artifacts
#-------------------------------------------------------------------------------
DIR<libc>:=$(DIR)include/
BIN<usr/root>:=$(BUILDDIR)usr/root.elf

#-------------------------------------------------------------------------------
# Source discovery
#-------------------------------------------------------------------------------
LDSCRIPT<$(BIN<usr/root>)>:=$(DIR)linker.ld
SRCS_C<$(BIN<usr/root>)>:=$(wildcard $(DIR)*.c)
SRCS_ASM<$(BIN<usr/root>)>:=$(wildcard $(DIR)*.asm)

#-------------------------------------------------------------------------------
# Rules
#-------------------------------------------------------------------------------
CC<$(BIN<usr/root>)>:=$(CC<target/amd64>)
CFLAGS<$(BIN<usr/root>)>:=$(CFLAGS<target/amd64>) \
    -ffreestanding
CPPFLAGS<$(BIN<usr/root>)>:=$(CPPFLAGS<target/amd64>) \
    -nostdlibinc \
    -I$(INCLUDE<libc>) \
    -DIS_KERNEL=0
AS<$(BIN<usr/root>)>:=$(AS<target/amd64>)
ASFLAGS<$(BIN<usr/root>)>:=$(ASFLAGS<target/amd64>)
LDFLAGS<$(BIN<usr/root>)>:=$(LDFLAGS<target/amd64>) \
    -Wl,-T$(LDSCRIPT<$(BIN<usr/root>)>) \
    -Wl,-z -Wl,max-page-size=4096
LDLIBS<$(BIN<usr/root>)>:=-nostdlib
$(eval $(call spawn_c_rule,$(BIN<usr/root>)))
$(eval $(call spawn_asm_rule,$(BIN<usr/root>)))
$(eval $(call spawn_c_link_rule,$(BIN<usr/root>)))
