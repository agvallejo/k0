section .text
global _main
_main:
	extern __ld_stack_start
	lea rsp, [__ld_stack_start]
	mov rbp, rsp

	extern main
	call main
